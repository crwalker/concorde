const webpack = require('webpack');
const path = require('path');
const pkg = require('./package.json');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const assert = require('assert');

assert(process.env.CONCORDE_OUTPUT_DIR, 'CONCORDE_OUTPUT_DIR env var not set!');
assert(process.env.CONCORDE_HOST, 'CONCORDE_HOST env var not set!');
assert(process.env.CONNECTOR_HOST, 'CONNECTOR_HOST env var not set!');
assert(process.env.CONNECTOR_WS_PORT, 'CONNECTOR_WS_PORT env var not set!');

const APP_DIR = path.join(__dirname, 'app');
const OUTPUT_DIR = process.env.CONCORDE_OUTPUT_DIR;

module.exports = {
    entry: {
        app: [
            'babel-polyfill',
            'react-hot-loader/patch',
            path.join(APP_DIR, 'index.js'),
        ],
        // app: [
        //     'babel-polyfill',
        //     'react-hot-loader/patch',
        //     path.join(APP_DIR, 'app.js'),
        // ],
        lib: [
            'react',
            'react-dom',
            'react-bootstrap',
            'ramda',
            'core-js',
        ],
    },
    module: {
        rules: [
            {
                test: /\.js?/,
                include: APP_DIR,
                loader: 'babel-loader',
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            // {
            //     test: /\.css$/,
            //     include: APP_DIR,
            //     loader: ExtractTextPlugin.extract({
            //         loader: 'css-loader',
            //         fallbackLoader: 'style-loader',
            //     }),
            // },
            {
                test: /\.(jpg|png|gif)$/,
                loader: 'file-loader',
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    limit: 1000,
                    mimetype: 'application/font-woff',
                },
            },
            {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    limit: 1000,
                    mimetype: 'application/font-woff2',
                },
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    limit: 1000,
                    mimetype: 'application/octet-stream',
                },
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader',
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    limit: 1000,
                    mimetype: 'image/svg+xml',
                },
            },
        ],
    },
    output: {
        path: OUTPUT_DIR,
        publicPath: '/',
        filename: '[name].js',
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'process.env' : {
                VERSION: JSON.stringify(pkg.version),
                SST_HOST: JSON.stringify(process.env.SST_HOST),
                CONCORDE_HOST: JSON.stringify(process.env.CONCORDE_HOST),
                CONNECTOR_HOST: JSON.stringify(process.env.CONNECTOR_HOST),
                CONNECTOR_WS_PORT: JSON.stringify(process.env.CONNECTOR_WS_PORT),
            },
        }),
        new ExtractTextPlugin('concorde.css'),
        new FaviconsWebpackPlugin({
            logo: path.join(APP_DIR, 'assets/img/favicon.png'),
            title: 'Concorde',
            inject: true,
        }),
        // new HtmlWebpackPlugin({
        //     inject: 'body',
        //     hash: false,
        //     template: path.join(APP_DIR, 'concorde.html'),
        //     filename: path.join(OUTPUT_DIR, 'concorde.html'),
        // }),
        new HtmlWebpackPlugin({
            inject: 'body',
            hash: false,
            template: path.join(APP_DIR, 'index.html'),
            filename: path.join(OUTPUT_DIR, 'index.html'),
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['lib', 'manifest'],
            minChunks: Infinity,
        }),
    ],

    // https://medium.com/@rajaraodv/webpacks-hmr-react-hot-loader-the-missing-manual-232336dc0d96
    // https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9
    devServer: {
        inline: true,
        hot: true,
        contentBase: OUTPUT_DIR,
        historyApiFallback: {app: 'index.html'},
        headers: {'Access-Control-Allow-Origin': '*'},
        public: '0.0.0.0:8080',
    },
};
