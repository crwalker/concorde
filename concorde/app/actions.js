import {createAction} from 'redux-actions';
import R from 'ramda';

import utils from '@/utils';
import constants from '@/constants';
import {identitySignature, identitySignatureValid, identityKey, identityHash, valueKey} from '@/crypto';

// these actions are intercepted by socketMiddleware (see store.js, socket.js)
const apiStartWebsocket = createAction('API_START_WEBSOCKET');
const apiSendWebsocket = createAction('API_SEND_WEBSOCKET');

// actions that modify store through reducers
const setClientKeys = createAction('SET_CLIENT_ID');
const startMessage = createAction('START_MESSAGE');
const addResponse = createAction('ADD_RESPONSE');
const endMessage = createAction('END_MESSAGE');
const addName = createAction('ADD_NAME');
const addQueuedKey = createAction('ADD_QUEUED_KEY');
const removeQueuedKey = createAction('REMOVE_QUEUED_KEY');
const toggleAside = createAction('TOGGLE_ASIDE');
const toggleSidebar = createAction('TOGGLE_SIDEBAR');
const receiveEntities = createAction('RECEIVE_ENTITIES');

// asynchronous actions
const send = opts => (dispatch, getState) => {
    const message = {
        opts: R.merge(
            opts,
            {
                clientKey: getState().app.clientKey,
                // every message has a random identifier
                key: utils.randomKey(),
            },
        ),
        responses: [],
        ended: false,
    };

    dispatch(startMessage(message));

    dispatch(apiSendWebsocket(message.opts));

    utils.delayPromise(constants.SEARCH_TIMEOUT).then(
        () => dispatch(endMessage(message)),
    );
};

const storeValue = opts => dispatch => new Promise((resolve) => {
    console.assert(opts.data && opts.dataType, 'opts must include data and dataType keys');
    dispatch(send({
        type: 'entryStore',
        entries: [R.merge(
            {entryType: 'value'},
            opts,
        )],
    }));

    // pretend the value was stored correctly immediately
    // TODO(cw): do not pretend!
    resolve(R.merge(
        opts,
        {
            key: valueKey(opts),
            live: true,
            _type: 'value',
        },
    ));
});

const version = (entity, newValue) => (dispatch, getState) => {
    // make an identity including both the existing entity and the new value.

    let identity;

    const {publicKey, secretKey} = getState().app;

    if (entity._type === 'value') {
        // create a new identity based on this value
        identity = {
            entryType: 'identity',
            value: utils.base16ToArray(newValue.key),
            publicKey,
        };

        identity.signature = identitySignature(identity, secretKey);
        identity.key = identityKey(identity);
    } else if (entity._type === 'identity') {
        // create a new identity entry for this new version of the value
        identity = {
            entryType: 'identity',
            value: utils.base16ToArray(newValue.key),
            previous: identityHash(entity),
            publicKey,
        };

        identity.signature = identitySignature(identity, secretKey);
        identity.key = entity.key;
    } else {
        console.error('unkown entity type');
        return;
    }

    dispatch(send({
        type: 'entryStore',
        entries: [identity],
    }));
};

// actions that use localStorage
const logout = () => () => {
    localStorage.removeItem('concorde-public-key');
    localStorage.removeItem('concorde-secret-key');
    localStorage.removeItem('concorde-accept-tc');
    localStorage.clear();
    location.reload(true);
};

// save a key to local storage so that it may be fetched later
const localSave = ({key, value}) => () => {
    console.warn(`saving to local storage: (${key}, ${value})`);
    localStorage.setItem(key, value);
};

// save a key to local storage so that it may be fetched later
const loadKeys = () => (dispatch) => {
    let nKeys = 0;

    R.mapObjIndexed(
        (value, key) => {
            if (utils.isKey(key)) {
                console.log('sending search for key', key);
                dispatch(send({
                    type: 'search',
                    targetKey: key,
                }));
                nKeys++;
            }
        },
        localStorage,
    );

    return nKeys;
};

export {
    apiStartWebsocket,
    apiSendWebsocket,
    addQueuedKey,
    storeValue,
    removeQueuedKey,
    send,
    setClientKeys,
    receiveEntities,
    addName,
    startMessage,
    addResponse,
    endMessage,
    toggleAside,
    toggleSidebar,
    logout,
    localSave,
    loadKeys,
    version,
};
