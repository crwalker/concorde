import React from 'react';
import {NavLink} from 'react-router-dom';

const TransportSidebar = () => (
    <div className="sidebar">
        <nav className="sidebar-nav">
            <ul className="nav">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app">
                        <i className="icon-home" />
                        Home
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">Transport</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/">
                        <i className="icon-speedometer" />
                        Dashboard
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/search/">
                        <i className="icon-magnifier" />
                        Search
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">My Resources</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/customer">Customers</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/supplier">Suppliers</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/shipment">Shipments</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/rfq">RFQs</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/bol">Bills of Lading</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/transport/invoice">Invoices</NavLink>
                </li>
            </ul>
        </nav>
    </div>
);

export default TransportSidebar;
