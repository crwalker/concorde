import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import {Row, Col} from 'react-bootstrap';

import utils from '@/utils';
import constants from '@/constants';
import Tx from '@/components/Tx';
import EntryButton from '@/components/EntryButton';

const Block = ({block}) => (
    <div>
        <Row>
            <Col sm={6}>
                <h3 className="card-title">Block {block.height}</h3>
            </Col>
            <Col sm={6}>
                <div className="float-right">
                    <EntryButton k={block.key} />
                </div>
            </Col>
        </Row>
        <hr />
        <Row>
            <Col md={4}>
                <ul className="icons-list">
                    <li>
                        <i className="icon-location-pin bg-primary" />
                        <div className="desc">
                            <div className="title">Hash</div>
                            <small>Each block hash is unique</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">&nbsp;</div>
                            <strong>{utils.arr2display(block.hash)}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                    <li>
                        <i className="icon-layers bg-info" />
                        <div className="desc">
                            <div className="title">Block height</div>
                            <small>The highest block is the most recent</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">&nbsp;</div>
                            <strong>{block.height}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                    <li>
                        <i className="icon-link bg-info" />
                        <div className="desc">
                            <div className="title">Previous Hash</div>
                            <small>Reference to prior block</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">&nbsp;</div>
                            <strong>{utils.arr2display(block.previousHash)}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                    <li>
                        <i className="icon-key bg-info" />
                        <div className="desc">
                            <div className="title">Merkle Root</div>
                            <small>Tree formed by tx hashes</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">&nbsp;</div>
                            <strong>{utils.arr2display(block.merkleRoot)}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                    <li>
                        <i className="icon-key bg-info" />
                        <div className="desc">
                            <div className="title">Storage Proof</div>
                            <small>Hash demonstrating proof of storage</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">&nbsp;</div>
                            <strong>{utils.arr2display(block.storageProof)}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                    <li>
                        <i className="icon-key bg-info" />
                        <div className="desc">
                            <div className="title">Proof of Work</div>
                            <small>Nonce meeting hash conditions</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">&nbsp;</div>
                            <strong>{block.nonce}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                    <li>
                        <i className="icon-clock bg-info"></i>
                        <div className="desc">
                            <div className="title">Time</div>
                            <small>&nbsp;</small>
                        </div>
                        <div className="value">
                            <div className="small text-muted">Timestamp of block mining</div>
                            <strong>{utils.time2str(block.time)}</strong>
                        </div>
                        <div className="actions">
                        </div>
                    </li>
                </ul>
            </Col>
            <Col md={8}>
                {R.values(R.mapObjIndexed(
                    (tx, index) => <Tx tx={tx} key={index} index={parseInt(index, 10)} />,
                    block.txs,
                ))}
            </Col>
        </Row>
    </div>
);

Block.propTypes = {
    block: PropTypes.shape(constants.BLOCK_SHAPE),
};

export default Block;
