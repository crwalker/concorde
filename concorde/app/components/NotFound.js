import React from 'react';

import Card from '@/components/Card';
import Breadcrumb from '@/components/Breadcrumb';
import PageHeader from '@/components/PageHeader';
import HomeSidebar from '@/components/HomeSidebar';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';
import Aside from '@/components/Aside';

const NotFound = () => (
    <div className="app-body">
        <HomeSidebar />
        <main className="main">
            <div className="container-fluid" style={{marginTop: 100}}>
                <div className="card card-inverse card-danger">
                    <Card.Body>
                        Does not exist (yet)
                    </Card.Body>
                </div>
            </div>
        </main>
        <Aside>
            <QueuedKeys />
            <Auth />
        </Aside>
    </div>
);

export default NotFound;
