import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';

import utils from '@/utils';
import constants from '@/constants';
import {logout} from '@/actions';

const Auth = props => (
    <div>
        <div className="callout m-0 py-h text-muted text-center bg-faded text-uppercase">
            <i className="icon-key" style={{color: '#20a8d8'}} />&nbsp;Secret Client Key
        </div>
        <div className="callout callout-danger m-0 py-1">
            <form>
                <div className="form-group mb-0">
                    <textarea
                        readOnly
                        className="form-control"
                        value={utils.arrToBase16(props.clientKey)}
                        style={{wordWrap: 'break-word', backgroundColor: 'white'}}
                        rows={3}
                    />
                </div>
            </form>
        </div>
        <div className="callout callout-danger text-center m-0 py-1">
            <button onClick={props.logout} className="btn btn-danger btn-inverse">Sign off</button>
            <div>
                <small className="text-muted">
                    This will permanently erase your client key:
                    ensure you have a backup if you want to use
                    this data again
                </small>
            </div>
        </div>
    </div>
);

const mapStateToProps = state => ({
    clientKey: R.clone(state.app.clientKey),
});

Auth.propTypes = {
    logout: PropTypes.func.isRequired,
    clientKey: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, {logout})(Auth);
