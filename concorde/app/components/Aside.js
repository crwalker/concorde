import React from 'react';
import PropTypes from 'prop-types';

export default props => (
    <aside className="aside-menu">
        <div className="tab-content">
            {props.children}
        </div>
    </aside>
);
