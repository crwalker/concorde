import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {DropdownButton, Dropdown, MenuItem} from 'react-bootstrap';
import R from 'ramda';
import RootCloseWrapper from 'react-overlays/lib/RootCloseWrapper';
import ReactRootWrapper from 'react-overlays/lib/RootCloseWrapper';
import cx from 'classnames';

import constants from '@/constants';
import apps from '@/apps';
import {toggleAside, toggleSidebar} from '@/actions';
import {CustomToggle, CustomMenu} from '@/components/NavDropdown';


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    onClose = () => this.setState({open: false});
    onToggle = state => this.setState({open: !state.open});

    render() {
        const activeApp = apps[this.props.active];
        return (
            <header className="app-header navbar">
                <button className="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>
                <Link className="navbar-brand" to="/" style={{backgroundColor: 'inherit', borderBottom: '1px solid #D1D4D7'}} />
                <ul className="nav navbar-nav hidden-md-down">
                    <li className="nav-item">
                        <span className="nav-link navbar-toggler" onClick={this.props.toggleSidebar}>
                            ☰
                        </span>
                    </li>
                </ul>
                <ul className="nav navbar-nav ml-auto">
                    <li className="nav-item dropdown show">
                        <RootCloseWrapper
                            disabled={false}
                            onRootClose={this.onClose}
                        >
                            <Dropdown
                                id="foo"
                                open={this.state.open}
                                onToggle={this.onToggle}
                            >
                                <CustomToggle bsRole="toggle">
                                    {activeApp ? activeApp.title : 'Apps'}
                                </CustomToggle>
                                <CustomMenu bsRole="menu">
                                    <div className="dropdown-header text-center">
                                        <strong>Apps</strong>
                                    </div>
                                    {R.map(
                                        app => (
                                            <Link
                                                className={cx('dropdown-item', {active: app.key === this.props.active})}
                                                to={`/${app.url}`}
                                                key={app.key}
                                                onClick={this.onClose}
                                            >
                                                {app.icon}
                                                {app.title}
                                            </Link>
                                        ),
                                        R.values(apps),
                                    )}
                                </CustomMenu>
                            </Dropdown>
                        </RootCloseWrapper>
                    </li>
                    <li className="nav-item hidden-md-down">
                        <span className="nav-link navbar-toggler" onClick={this.props.toggleAside}>
                            ☰
                            {this.props.queuedKeyCount > 0 ?
                                <span className="badge badge-pill badge-info">{this.props.queuedKeyCount}</span>
                                :
                                null
                            }
                        </span>
                    </li>
                </ul>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    queuedKeyCount: state.app.keyQueue.length,
});

Header.propTypes = {
    active: PropTypes.string,
    toggleAside: PropTypes.func.isRequired,
    toggleSidebar: PropTypes.func.isRequired,
    queuedKeyCount: PropTypes.number.isRequired,
};

export default connect(mapStateToProps, {toggleAside, toggleSidebar})(Header);
