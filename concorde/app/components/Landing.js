import React from 'react';
import R from 'ramda';
import {Grid, Row, Col, Button, ButtonGroup} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import {withRouter} from 'react-router-dom';

import concorde from '@/assets/img/concorde.svg';
import citynight from '@/assets/img/citynight.jpg';
import glass from '@/assets/img/glass.jpg';

import MailingList from '@/components/MailingList';
import Tile from '@/components/Tile';
import HoverTip from '@/components/HoverTip';


class Landing extends React.Component {
    render() {
        return (
            <div>
                <Grid fluid>
                    <Row>
                        <Col sm={6}>
                            <Tile
                                style={{
                                    minHeight: '100vh',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                }}
                            >
                                <h1 style={{fontSize: '100px'}}>
                                    <b>SST</b>
                                </h1>
                                <p>
                                    <b>
                                        a single source of truth for important data
                                    </b>
                                </p>
                                <p>
                                    SST creates a secure data commons, enabling organizations to collaborate
                                    on any data at any scale with more trust and less cost.
                                </p>
                                <p>
                                    SST is an open protocol powered by blockchain, peer to peer networks,
                                    and modern cryptography. The test network is operational today
                                    and ready for building proof-of-concept applications.
                                </p>
                                <ButtonGroup>
                                    <Button href="#use" className="btn btn-primary">
                                        Using SST
                                    </Button>
                                    <Button href="#build" className="btn btn-secondary">
                                        Building the SST Network
                                    </Button>
                                </ButtonGroup>
                            </Tile>
                        </Col>
                        <Col sm={6}>
                            <Tile
                                style={{
                                    minHeight: '100vh',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                }}
                            >
                                <img alt="sst logo" src={concorde} style={{width: '100%', height: 'auto'}} />
                            </Tile>
                        </Col>
                    </Row>
                    <Row id="use">
                        <Col sm={6}>
                            <Tile
                                style={{
                                    backgroundImage: `url(${citynight})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center top',
                                }}
                            >
                                <p
                                    style={{
                                        fontSize: '1.75rem',
                                        fontWeight: 800,
                                        color: 'white',
                                    }}
                                >
                                    <i>
                                        SST is open infrastructure
                                        for building the next generation of global software systems
                                    </i>
                                </p>
                                <MailingList />
                                <br />
                                <p
                                    style={{
                                        fontSize: '1.75rem',
                                        fontWeight: 800,
                                        color: 'white',
                                    }}
                                >
                                    <i>
                                        Let's discuss potential applications
                                    </i>
                                </p>
                                <Button
                                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                                    href="mailto:chris@getsst.com?subject=SST consulting"
                                    className="btn btn-primary"
                                >
                                    Contact
                                </Button>
                            </Tile>
                        </Col>
                        <Col sm={6}>
                            <Tile background="white">
                                <h2>
                                    Strategic Advantages
                                </h2>
                                <b>
                                    New Economic Territory
                                </b>
                                <p>
                                    SST unlocks new opportunities for building unified digital systems.
                                </p>
                                <ul>
                                    <li><b>Manufacturing</b>: Industry 4.0 and the digital thread</li>
                                    <li><b>Transportation</b>: Provenance, chain of custody, and public infrastructure for autonomous vehicles</li>
                                    <li><b>IoT</b>: the digital twin</li>
                                </ul>
                                <p>
                                    Protocols like Ethernet and HTTP created vast new realms of
                                    economic activity by simplifying data communication. SST continues this trend by simplifying
                                    data storage, versioning, and collaboration.
                                </p>
                                <b>
                                    Agility
                                </b>
                                <p>
                                    If anything in the business environment can change rapidly, software must be able to
                                    keep up. SST has the flexibility to handle any kind of data, and software platforms that
                                    use SST to hold data are far simpler to integrate: they already speak
                                    the same language and can access the same data.
                                </p>
                                <b>
                                    Freedom
                                </b>
                                <p>
                                    SaaS platforms capture fragments of your data,
                                    and then lock the data up into their own data silo.
                                    They know how to extract value from this data, but because there is no
                                    open standard that SaaS platforms use to communicate with each other, it's hard to get
                                    a holistic view of your own data.
                                </p>
                                <p>
                                    SST turns the tables by letting you use your own data as you see fit. If your data is
                                    stored in SST, SaaS providers cannot hold it hostage.
                                </p>
                            </Tile>
                        </Col>
                    </Row>
                    <Row id="build">
                        <Col sm={6}>
                            <Tile>
                                <h2>
                                    Architecture
                                </h2>
                                <p>
                                    SST enables participating nodes to create a universal key-value store
                                    which reaches consensus by blockchain, verifies storage transactions using
                                    commutative hashing, and secures values by public-key cryptography, forming
                                    a distributed storage network. Value is transferred
                                    using a general purpose built-in cryptographic token.
                                </p>
                                <h2>
                                    FAQ
                                </h2>
                                <b>What are the long-term plans for SST?</b>
                                <p>
                                    We are releasing an open reference implementation of SST and working with
                                    pilot partners running proof-of-concept applications on top of SST. We want
                                    SST to become an open standard that every computer can understand and use.
                                </p>
                                <b>How can I get involved?</b>
                                <ul>
                                    <li>
                                        <b>Leverage</b>: Build something the world has never seen on top of SST.
                                    </li>
                                    <li>
                                        <b>Develop</b>: We want to hear from brilliant software developers excited about
                                        understanding and improving SST.
                                    </li>
                                    <li>
                                        <b>Host</b>: SST incentivizes nodes storing data using a crypto token like Bitcoin.
                                        Because the network is new, now is the best time to set up an SST node.
                                    </li>
                                </ul>
                                <b>How is SST unique?</b>
                                <p>
                                    SST lets us do new things. It is the most promising project building a secure
                                    data commons with the right feature set for solving real-world problems.
                                </p>
                            </Tile>
                        </Col>
                        <Col sm={6}>
                            <Tile
                                style={{
                                    backgroundImage: `url(${glass})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center center',
                                }}
                            >
                                <Button
                                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                                    className="btn btn-warning"
                                    onClick={() => this.props.history.push('/app')}
                                >
                                    Demo SST
                                </Button>
                                <br />
                                <br />
                                <Button
                                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                                    href="mailto:chris@getsst.com?subject=Building the SST Network"
                                    className="btn btn-warning"
                                >
                                    Email
                                </Button>
                                <br />
                                <br />
                                <Button
                                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                                    href="mailto:chris@getsst.com?subject=Requesting SST Slack Group Access"
                                    className="btn btn-warning"
                                >
                                    Slack
                                </Button>
                                <br />
                                <br />
                                <Button
                                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                                    href="mailto:chris@getsst.com?subject=Requesting SST git repo access"
                                    className="btn btn-warning"
                                >
                                    Git
                                </Button>
                                <br />
                                <br />
                                <Button
                                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                                    href="mailto:chris@getsst.com?subject=Requesting SST whitepaper"
                                    className="btn btn-warning"
                                >
                                    Whitepaper
                                </Button>
                            </Tile>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6}>
                            <Tile background="#ff5444">
                                <span style={{color: 'white'}}>
                                        Made with resolve in Palo Alto, CA
                                        <br />
                                        © 2017 Chris Walker, all rights reserved
                                </span>
                            </Tile>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

Landing.propTypes = {
    history: React.PropTypes.shape({
        push: React.PropTypes.func.isRequired,
    }).isRequired,
};

export default withRouter(Landing);
