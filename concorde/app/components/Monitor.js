import React from 'react';
import PropTypes from 'prop-types';
import {Switch, Route, Link} from 'react-router-dom';

import MonitorSidebar from '@/components/MonitorSidebar';
import MonitorDashboard from '@/components/MonitorDashboard';
import MonitorBlockchain from '@/components/MonitorBlockchain';
import MonitorNodes from '@/components/MonitorNodes';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';


class Monitor extends React.Component {
    render() {
        return (
            <div className="app-body">
                <MonitorSidebar />
                <Switch>
                    <Route exact path="/app/monitor" component={MonitorDashboard} />
                    <Route path="/app/monitor/blockchain" component={MonitorBlockchain} />
                    <Route path="/app/monitor/node" component={MonitorNodes} />
                </Switch>
                <Aside>
                    <QueuedKeys />
                    <Auth />
                </Aside>
            </div>
        );
    }
}

export default Monitor;

// export default (props) => <div>{props.children}</div>;

