import React from 'react';
import Spinner from 'react-spinkit';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';
import {Row, Col} from 'react-bootstrap';

import constants from '@/constants';
import utils from '@/utils';
import {send} from '@/actions';
import {selectEntities} from '@/selectors';
import Card from '@/components/Card';
import EntryButton from '@/components/EntryButton';
import Loading from '@/components/Loading';
import Block from '@/components/Block';
import Node from '@/components/Node';
import NoteView from '@/components/NoteView';


const Display = ({entity}) => {
    if (entity._type === 'block') {
        return <Block block={entity} />;
    } else if (entity._type === 'node') {
        return <Node node={entity} />;
    } else if (entity._type === 'value') {
        if (entity.dataType === 'markdown') {
            return (
                <div>
                    <h3>Note</h3>
                    <NoteView note={entity} />
                </div>
            );
        }
    }

    return <span>found entity with unknown data type</span>;
};

Display.propTypes = {
    entity: PropTypes.object.isRequired,
};


class Explorer extends React.Component {
    render() {
        const content = R.values(R.map(
            (key) => {
                let keyResults;
                if (this.props.entities[key]) {
                    keyResults = (
                        <Display key={key} entity={this.props.entities[key]} />
                    );
                } else if (R.any(
                    s => s.opts.targetKey === key && !s.ended,
                    this.props.searches,
                )) {
                    keyResults = <Loading />;
                } else {
                    keyResults = <span>Nothing found</span>;
                }

                return (
                    <Card key={key} >
                        <Card.Body>
                            {keyResults}
                        </Card.Body>
                    </Card>
                );
            },
            this.props.keys,
        ));

        return (
            <div>
                {content}
            </div>
        );
    }
}

Explorer.defaultProps = {
    searching: false,
};

Explorer.propTypes = {
    keys: PropTypes.objectOf(PropTypes.string).isRequired,
    send: PropTypes.func.isRequired,
    searches: PropTypes.arrayOf(PropTypes.shape(constants.MESSAGE_SHAPE)).isRequired,
    entities: PropTypes.objectOf(PropTypes.object).isRequired,
};

const mapStateToProps = (state, ownProps) => ({
    entities: selectEntities(state, ownProps.keys),
    searches: R.values(R.filter(
        message => message.opts.type === 'search',
        state.app.message,
    )),
});

export default connect(mapStateToProps, {send})(Explorer);
