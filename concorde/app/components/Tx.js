import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-bootstrap';
import R from 'ramda';

import utils from '@/utils';
import constants from '@/constants';

const Tx = props => (
    <div>
        <h4>Transaction {props.index}</h4>
        <hr />
        <div>
            <h5>Inputs</h5>
            <table className="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <td>Asset</td>
                        <td>Value</td>
                        <td><i className="fa fa-code" />&nbsp;Script</td>
                    </tr>
                </thead>
                <tbody>
                    {R.values(R.mapObjIndexed(
                        (input, index) => (
                            <tr key={index} >
                                <td>
                                    {utils.arr2display(input.asset)}
                                </td>
                                <td>
                                    {input.amount}
                                </td>
                                <td>
                                    {utils.data2display(input.script.data)}
                                </td>
                            </tr>
                        ),
                        props.tx.inputs,
                    ))}
                </tbody>
            </table>
        </div>
        <div>
            <h5>Outputs</h5>
            <table className="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <td>Asset</td>
                        <td>Value</td>
                        <td><i className="fa fa-code" />&nbsp;Script</td>
                    </tr>
                </thead>
                <tbody>
                    {R.values(R.mapObjIndexed(
                        (output, index) => (
                            <tr key={index} >
                                <td>
                                    {utils.arr2display(output.asset)}
                                </td>
                                <td>
                                    {output.amount}
                                </td>
                                <td>
                                    {utils.data2display(output.script.data)}
                                </td>
                            </tr>
                        ),
                        props.tx.outputs,
                    ))}
                </tbody>
            </table>
        </div>
    </div>
);

Tx.propTypes = {
    tx: PropTypes.shape(constants.TX_SHAPE).isRequired,
    index: PropTypes.number.isRequired,
};

export default Tx;
