import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-bootstrap';
import R from 'ramda';
import {connect} from 'react-redux';

import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Block from '@/components/Block';
import Card from '@/components/Card';
import Explorer from '@/components/Explorer';
import PageHeader from '@/components/PageHeader';
import FindSidebar from '@/components/FindSidebar';
import {selectEntities, runningSearches} from '@/selectors';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';

const Values = props => (
    <div className="app-body">
        <FindSidebar />
        <main className="main">
            <PageHeader>
                <h1 className="h2 page-title">Values</h1>
                <div className="text-muted page-desc">Save anything with a constant value</div>
            </PageHeader>
            <Breadcrumb
                path={[
                    {name: 'Home', url: '/'},
                    {name: 'Find', url: '/app/find'},
                    {name: 'Values'},
                ]}
            />
            <div className="container-fluid">
                <Explorer keys={props.keys} />
            </div>
        </main>
        <Aside>
            <QueuedKeys />
            <Auth />
        </Aside>
    </div>
);

Values.propTypes = {
    keys: PropTypes.objectOf(PropTypes.string).isRequired,
};

const mapStateToProps = state => ({
    keys: R.map(
        value => value.key,
        state.entity.value,
    ),
    searches: runningSearches(state, R.keys(state.app.name)),
});

export default connect(mapStateToProps)(Values);
