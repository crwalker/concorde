import React from 'react';
import {Route, Switch} from 'react-router-dom';

import Landing from '@/components/Landing';
import Browser from '@/components/Browser';
import NotFound from '@/components/NotFound';


class App extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Landing} />
                <Route path="/app/" component={Browser} />
                <Route component={NotFound} />
            </Switch>
        );
    }
}

export default App;
