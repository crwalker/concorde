import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import R from 'ramda';

import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Node from '@/components/Node';
import Loading from '@/components/Loading';


class MonitorNodes extends React.Component {
    render() {
        return (
            <main className="main">
                <PageHeader>
                    <h1 className="h2 page-title">Nodes</h1>
                    <div className="text-muted page-desc">Computers participating in the SST network</div>
                </PageHeader>
                <Breadcrumb
                    path={[
                        {name: 'Home', url: '/'},
                        {name: 'Monitor SST', url: '/app/monitor'},
                        {name: 'Nodes'},
                    ]}
                />
                <div className="container-fluid">
                    {
                        R.isEmpty(this.props.nodes) ?
                            <Loading />
                            :
                            (<Row>
                                {R.map(
                                    node => (
                                        <Col sm={12} lg={6} key={node.key}>
                                            <Node node={node} key={node.key} />
                                        </Col>
                                    ),
                                    this.props.nodes,
                                )}
                            </Row>)
                    }
                </div>
            </main>
        );
    }
}

const mapStateToProps = state => ({
    nodes: R.sort(
        (a, b) => a.key - b.key,
        R.values(state.entity.node),
    ),
});

MonitorNodes.propTypes = {
    nodes: PropTypes.arrayOf(
        PropTypes.shape(
            constants.NODE_SHAPE,
        ),
    ),
};

export default connect(mapStateToProps)(MonitorNodes);
