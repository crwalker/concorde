
import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Row, Col, Form, ControlLabel, FormGroup, InputGroup, FormControl, HelpBlock, Button} from 'react-bootstrap';
import R from 'ramda';
import ReactMarkdown from 'react-markdown';


import {send, storeValue, version} from '@/actions';
import PageHeader from '@/components/PageHeader';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import History from '@/components/History';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';
import NoteSidebar from '@/components/NoteSidebar';
import Paginator from '@/components/Paginator';
import constants from '@/constants';
import Loading from '@/components/Loading';
import NoteView from '@/components/NoteView';
import utils from '@/utils';


class NoteCompose extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            text: props.value ? props.value.data : '',
        };
    }

    onChange = (event) => {
        this.setState({text: event.target.value});
    };

    onSubmit = (event) => {
        event.preventDefault();
        this.props.storeValue({
            data: this.state.text,
            dataType: 'markdown',
        }).then(
            (newValue) => {
                if (this.props.value) {
                    // there was an existing value: create an identity
                    this.props.version(this.props.value, newValue);
                }
                this.props.history.push('/app/note/');
            },
        );
    }

    // TODO(cw): clean up race conditions properly
    componentWillReceiveProps(nextProps) {
        if (nextProps.value && !this.props.value) {
            this.setState({text: nextProps.value.data});
        }
    }

    render() {
        const searches = R.filter(
            message => (!message.ended && R.isEmpty(message.responses) && message.opts.value === this.state.text),
            this.props.storeValueMessages,
        );

        let content;

        if (R.isEmpty(searches)) {
            content = (
                <Row>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <h4>
                                    {this.props.value ? 'Edit Note' : 'New Note'}
                                </h4>
                                <form onSubmit={this.onSubmit}>
                                    <FormGroup
                                        controlId="formControlsTextarea"
                                    >
                                        <InputGroup>
                                            <FormControl
                                                componentClass="textarea"
                                                value={this.state.text}
                                                onChange={this.onChange}
                                                rows={10}
                                            />
                                        </InputGroup>
                                        <HelpBlock>Notes use the <a href="http://commonmark.org/help/">markdown</a> format</HelpBlock>
                                    </FormGroup>
                                    <Button
                                        className="btn btn-primary btn-large centerButton"
                                        type="submit"
                                        disabled={R.isEmpty(this.state.text)}
                                        onSubmit={this.onSubmit}
                                    >
                                        {this.props.value ? 'Save' : 'Create New Note'}
                                    </Button>
                                </form>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6}>
                        {this.state.text.length > 0 ?
                            <Card>
                                <Card.Body>
                                    <ReactMarkdown source={this.state.text} escapeHtml />
                                </Card.Body>
                            </Card>
                            :
                            null
                        }
                    </Col>
                </Row>
            );
        } else {
            content = <Loading />;
        }

        return (
            <div className="app-body">
                <NoteSidebar />
                <main className="main">
                    <PageHeader>
                        <h1 className="h2 page-title">Note</h1>
                        <div className="text-muted page-desc">Minimal note-taking tool</div>
                    </PageHeader>
                    <Breadcrumb
                        path={[
                            {name: 'Home', url: '/'},
                            {name: 'Note', url: 'app/note'},
                            {name: 'Compose'},
                        ]}
                    />
                    <div className="container-fluid">
                        {content}
                    </div>
                </main>
                <Aside>
                    <QueuedKeys />
                    <Auth />
                </Aside>
            </div>
        );
    }
}

NoteCompose.defaultProps = {
    // k: '',
    value: null,
};

NoteCompose.propTypes = {
    // k: PropTypes.string,
    send: PropTypes.func.isRequired,
    storeValue: PropTypes.func.isRequired,
    version: PropTypes.func.isRequired,
    value: PropTypes.object,
    storeValueMessages: PropTypes.arrayOf(PropTypes.shape(constants.MESSAGE_SHAPE)).isRequired,
};

const mapStateToProps = (state, ownProps) => ({
    storeValueMessages: R.values(R.filter(
        message => (message.opts.type === 'entryStore'),
        state.app.message,
    )),
    value: state.entity.value[ownProps.match.params.k] || null,
});

export default withRouter(connect(mapStateToProps, {send, storeValue, version})(NoteCompose));
