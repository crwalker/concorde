import React from 'react';
import {ButtonGroup} from 'react-bootstrap';

export default () => (
    <footer className="app-footer">
        Concorde © 2017 Chris Walker
        <span className="float-right">
            <ButtonGroup bsSize="sm" >
                <a href="mailto:chris@ckwalker.com">
                    <button className="btn btn-link">
                        <i className="icon-envelope" />
                        <span style={{marginLeft:'5px'}}>Email</span>
                    </button>
                </a>
                <a href="https://sstorg.slack.com">
                    <button className="btn btn-link">
                        <i className="fa fa-slack" />
                        <span style={{marginLeft:'5px'}}>Chat</span>
                    </button>
                </a>
                <a href="https://gitlab.com/crwalker/sst">
                    <button className="btn btn-link">
                        <i className="fa fa-code-fork" />
                        <span style={{marginLeft:'5px'}}>SST</span>
                    </button>
                </a>
                <a href="https://gitlab.com/crwalker/concorde">
                    <button className="btn btn-link">
                        <i className="fa fa-code-fork" />
                        <span style={{marginLeft:'5px'}}>Concorde</span>
                    </button>
                </a>
            </ButtonGroup>
        </span>
    </footer>
);
