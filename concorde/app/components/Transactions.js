import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-bootstrap';

import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Block from '@/components/Block';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';

const Transactions = () => (
    <main className="main">
        <PageHeader>
            <h1>Transactions</h1>
            <p>Send Value Anywhere</p>
        </PageHeader>
        <Breadcrumb path={[{name: 'tx'}]} />
        <div className="container-fluid">
            <Row>
                <Col md={6}>
                    <Card>
                        <Card.Header>
                            <strong>Search for a Transaction</strong>
                        </Card.Header>
                    </Card>
                </Col>
            </Row>
        </div>
    </main>
);

export default Transactions;
