import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';

const Tile = props => (
    <div
        style={R.merge(
            {
                margin: '0 -15px',
                minHeight: '100%',
                padding: props.padding,
                background: props.background,
                flex: 1,
            },
                props.style,
            )
        }
    >
        {props.children}
    </div>
);

Tile.propTypes = {
    background: PropTypes.string,
    padding: PropTypes.string,
};

Tile.defaultProps = {
    background: '',
    padding: '80px',
};

export default Tile;
