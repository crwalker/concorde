import React from 'react';

export default ({children}) => (
    <div className="page-header">
        <div className="container-fluid">
            {children}
        </div>
    </div>
);