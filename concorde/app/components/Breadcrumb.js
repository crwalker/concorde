import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import {Link} from 'react-router-dom';
import cx from 'classnames';

const Breadcrumb = ({path}) => (
    <ol className="breadcrumb">
        {R.values(R.mapObjIndexed(
            (entry, index) => (
                <li
                    className={cx('breadcrumb-item', index == path.length - 1 ? 'active' : 'foo')}
                    key={index}
                >
                    {entry.url ? <Link to={entry.url}>{entry.name}</Link> : entry.name}
                </li>
            ),
            path,
        ))}
        {/*  <li className="breadcrumb-menu">
            <div className="btn-group">
                <a className="btn btn-secondary" href="#">
                    Add a value
                </a>
                <a className="btn btn-secondary" href="#">
                    Filter
                </a>
            </div>
        </li>*/}
    </ol>
);

Breadcrumb.propTypes = {
    path: PropTypes.arrayOf(PropTypes.shape({
        url: PropTypes.string,
        name: PropTypes.string.isRequired,
    })).isRequired,
};

export default Breadcrumb;
