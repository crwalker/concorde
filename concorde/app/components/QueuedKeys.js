import React from 'react';
import PropTypes from 'prop-types';
import {Form, ControlLabel, FormGroup, InputGroup, FormControl, HelpBlock, Button} from 'react-bootstrap';
import R from 'ramda';
import {connect} from 'react-redux';

import {removeQueuedKey} from '@/actions';
import utils from '@/utils';
import Tip from '@/components/Tip';
import EntryButton from '@/components/EntryButton';

const QueuedKeys = props => (
    <div>
        <div className="callout m-0 py-h text-muted text-center bg-faded text-uppercase">
            <i className="icon-location-pin" style={{color: '#20a8d8'}} />&nbsp;Selected Addresses
        </div>

        {R.isEmpty(props.keys) ?
            <div className="callout" />
            :
            null
        }

        {R.values(R.mapObjIndexed(
            (key, index) => (
                <div className="callout callout-info m-0 py-1" key={index} >
                    <EntryButton queued k={key} removeQueuedKey={() => removeQueuedKey(index)} />
                </div>
            ),
            props.keys,
        ))}
    </div>
);

QueuedKeys.propTypes = {
    keys: PropTypes.arrayOf(PropTypes.string),
    removeQueuedKey: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    keys: state.app.keyQueue,
});

export default connect(mapStateToProps, {removeQueuedKey})(QueuedKeys);
