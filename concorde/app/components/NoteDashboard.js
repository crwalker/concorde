import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {dataTypeHandlers} from '@/api';
import {send} from '@/actions';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';
import NoteSidebar from '@/components/NoteSidebar';
import NoteView from '@/components/NoteView';
import Paginator, {withPaginator} from '@/components/Paginator';


const NoteList = props => (

    <Card>
        <Card.Body>
            <h3>Notes</h3>
            {R.map(
                note => <NoteView note={note} key={note.key} />,
                props.notes,
            )}
            <Paginator.Pager {...props} />
        </Card.Body>
    </Card>
);

NoteList.propTypes = {
    keys: PropTypes.arrayOf(PropTypes.string).isRequired,
    notes: PropTypes.arrayOf(PropTypes.shape(constants.VALUE_SHAPE)).isRequired,
};

const WrappedNoteList = withPaginator(NoteList);


class NoteDashboard extends React.Component {
    componentWillMount() {
        // need to fetch notes somehow
    }

    render() {
        const noteList = R.isEmpty(this.props.notes) ?
            null
            :
            <WrappedNoteList keys={this.props.noteKeys} notes={this.props.notes} />;

        // const noteList = R.map(
        //     note => <NoteView note={note} />,
        //     this.props.notes,
        // );

        return (
            <div className="app-body">
                <NoteSidebar />
                <main className="main">
                    <PageHeader>
                        <h1 className="h2 page-title">Note</h1>
                        <div className="text-muted page-desc">Minimal note-taking tool</div>
                    </PageHeader>
                    <Breadcrumb
                        path={[
                            {name: 'Home', url: '/'},
                            {name: 'Note'},
                        ]}
                    />
                    <div className="container-fluid">
                        <Row>
                            <Col sm={6}>
                                <Card>
                                    <Card.Body>
                                        <h3>Welcome to Note</h3>
                                        <p>
                                            Note is an ultra light weight tool for writing new notes, saving them to SST,
                                            viewing the history of each note, and sharing notes with friends.
                                        </p>
                                        <p>
                                            Right now, notes are public, so follow the general principle for writing in the
                                            information age: only write things you are OK with seeing on the front page
                                            of the New York Times!
                                        </p>
                                        <Link to="/app/note/compose">
                                            <Button
                                                className="btn btn-outline-primary btn-default"
                                            >
                                                Compose New Note
                                            </Button>
                                        </Link>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col sm={6}>
                                {noteList}
                            </Col>
                        </Row>
                    </div>
                </main>
                <Aside>
                    <QueuedKeys />
                    <Auth />
                </Aside>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const notes = R.compose(
        // R.sort((a, b) => a.timestamp < b.timestamp),
        R.values,
        R.filter(entity => entity.dataType === dataTypeHandlers.markdown.name),
    )(state.entity.value);

    const noteKeys = R.map(
        entity => entity.key,
        notes,
    );

    return ({
        notes,
        noteKeys,
    });
};

NoteDashboard.propTypes = {
    send: PropTypes.func.isRequired,
    noteKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
    notes: PropTypes.arrayOf(PropTypes.shape(constants.VALUE_SHAPE)).isRequired,
};

export default connect(mapStateToProps, {send})(NoteDashboard);
