import React from 'react';
import PropTypes from 'prop-types';
import {elementType} from 'react-prop-types';
import R from 'ramda';

class Paginator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startIndex: 0,
        };
    }

    select = () => R.slice(this.state.startIndex, this.state.startIndex + this.props.pageSize, this.props.keys);

    previous = () => {
        this.setState({
            startIndex: R.max(0, this.state.startIndex - this.props.pageSize),
        });
    };

    next = () => {
        if (this.props.keys.length > this.state.startIndex + this.props.pageSize) {
            this.setState({startIndex: this.state.startIndex + this.props.pageSize});
        }
    };

    previousDisabled = () => this.state.startIndex === 0;
    nextDisabled = () => this.state.startIndex + this.props.pageSize > this.props.keys.length;

    render() {
        const Component = this.props.Component;
        return (
            <Component
                {...this.props}
                keys={this.select()}
                next={this.next}
                previous={this.previous}
                nextDisabled={this.nextDisabled}
                previousDisabled={this.previousDisabled}
                startIndex={this.state.startIndex}

            />
        );
    }
}

// const mapStateToProps = state => ({

// });

Paginator.defaultProps = {
    pageSize: 10,
};

Paginator.propTypes = {
    Component: elementType.isRequired,
    keys: PropTypes.arrayOf(PropTypes.string).isRequired,
    pageSize: PropTypes.number,
};

Paginator.Pager = props => (
    props.keys.length > 0 ?
        (<div style={{display: 'flex', justifyContent: 'center'}} >
            <ul className="pagination">
                <li className={props.previousDisabled() ? 'disabled' : ''}>
                    <a role="button" onClick={props.previous}>
                        <i className="fa fa-angle-left" />
                    </a>
                </li>
                <li className={props.nextDisabled() ? 'disabled' : ''}>
                    <a role="button" onClick={props.next}>
                        <i className="fa fa-angle-right" />
                    </a>
                </li>
            </ul>
        </div>)
        :
        null
);

Paginator.Pager.propTypes = {
    startIndex: PropTypes.number.isRequired,
    previous: PropTypes.func.isRequired,
    next: PropTypes.func.isRequired,
    nextDisabled: PropTypes.func.isRequired,
    previousDisabled: PropTypes.func.isRequired,
    keys: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export const withPaginator = Component => props => <Paginator {...props} Component={Component} />;

export default Paginator;

