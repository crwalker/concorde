import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import R from 'ramda';
import {Redirect} from 'react-router';
import QrReader from 'react-qr-reader';
import {Switch, Route, Link, withRouter} from 'react-router-dom';

import FindSidebar from '@/components/FindSidebar';
import FindDashboard from '@/components/FindDashboard';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Names from '@/components/Names';
import Identities from '@/components/Identities';
import Values from '@/components/Values';
import Explorer from '@/components/Explorer';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import {send} from '@/actions';
import utils from '@/utils';
import apps from '@/apps';
import Auth from '@/components/Auth';
import KeyInput from '@/components/KeyInput';

class Search extends React.Component {
    constructor(props) {
        console.log('constructor');
        super(props);
        this.state = {key: '', scan: false, scanVisible: false};
    }

    componentWillMount() {
        console.log('componentwillmount');
        const key = this.props.match.params.key;

        if (key) {
            this.setState({key});
        }

        if (key && utils.isKey(key)) {
            console.log('starting a search for the key in the url:', key);
            this.props.send({
                type: 'search',
                targetKey: key,
            });
        }
    }

    onChange = (key) => {
        this.setState({
            key,
            scan: this.state.scan && !utils.isKey(key),
        });
        if (utils.isKey(key) || key === '') {
            this.props.history.push(`/app/find/key/${key}`);
        }
    };

    onScan = (result) => {
        let key = this.state.key;

        if (result) {
            key = result.trim();

            this.setState({
                scanVisible: Boolean(result),
                key,
                scan: false,
            });
        }
    };

    onSubmit = (key) => {
        console.log('saving key to', key);

        this.props.send({
            type: 'search',
            targetKey: key,
        });

        this.setState({key});

        this.props.history.push(`/app/find/key/${key}`);
    };


    render() {
        let scanner = null;

        if (this.state.scan) {
            scanner = (
                <div>
                    <div style={{
                        padding: '1px',
                        background: this.state.scanVisible ? '#20add8' : '#d1d4d7',
                        display: 'inline-block',
                    }}
                    >
                        <div style={{overflow: 'hidden'}}>
                            <QrReader
                                delay={100}
                                style={{height: 200, width: 200, objectFit: 'cover'}}
                                onError={console.error}
                                onScan={this.onScan}
                            />
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className="app-body">
                <FindSidebar />
                <main className="main">
                    <PageHeader>
                        <h1 className="h2 page-title">Search</h1>
                        <div className="text-muted page-desc">Find anything in SST</div>
                    </PageHeader>
                    <Breadcrumb
                        path={[
                            {name: 'Home', url: '/'},
                            {name: 'Find', url: '/app/find'},
                            {name: 'By Key'},
                        ]}
                    />
                    <div className="container-fluid">
                        <Row>
                            <Col sm={6}>
                                <Card>
                                    <Card.Body>
                                        <KeyInput
                                            keyString={this.state.key}
                                            onChange={this.onChange}
                                            onSubmit={this.onSubmit}
                                            searches={this.props.searches}
                                        />
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col sm={6}>
                                <Card>
                                    <Card.Body>
                                        {this.state.scan ?
                                            scanner
                                            :
                                            <Button
                                                className="btn btn-primary btn-large centerButton"
                                                onClick={() => this.setState({scan: true, key: ''})}
                                            >
                                                Scan a QR Code
                                            </Button>
                                        }
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                        <Explorer
                            keys={this.state.key ? {[this.state.key]: this.state.key} : {}}
                        />
                    </div>
                </main>
                <Aside>
                    <QueuedKeys />
                    <Auth />
                </Aside>
            </div>
        );
    }
}

Search.propTypes = {
    match: PropTypes.object.isRequired,
    send: PropTypes.func.isRequired,
    searches: PropTypes.arrayOf(PropTypes.shape(constants.MESSAGE_SHAPE)).isRequired,
};

const mapStateToProps = state => ({
    searches: R.values(R.filter(
        message => message.opts.type === 'search',
        state.app.message,
    )),
});

export default withRouter(connect(mapStateToProps, {send})(Search));
