import React from 'react';
import PropTypes from 'prop-types';
import {Tooltip, OverlayTrigger} from 'react-bootstrap';

const Tip = ({tip, children}) => {
    const overlay = (
        <Tooltip>{tip}</Tooltip>
    );

    // span is needed so that the trigger has a single child.
    return (
        <OverlayTrigger placement="top" overlay={overlay}>
            {children}
        </OverlayTrigger>
    );
};

Tip.propTypes = {
    tip: PropTypes.node.isRequired,
};

export default Tip;
