import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';
import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {send} from '@/actions';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Block from '@/components/Block';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';

class MonitorDashboard extends React.Component {
    render() {
        return (
            <main className="main">
                <PageHeader>
                    <h1 className="h2 page-title">Monitor SST</h1>
                    <div className="text-muted page-desc">View the status of the SST network</div>
                </PageHeader>
                <Breadcrumb
                    path={[
                        {name: 'Home', url: '/'},
                        {name: 'Monitor SST'},
                    ]}
                />
                <div className="container-fluid">
                    <Row>
                        <Col sm={6}>
                            <Card>
                                <Card.Header>
                                    SST Network
                                </Card.Header>
                                <Card.Body>
                                    <Row>
                                        <Col sm={4}>
                                            <div className="callout callout-info">
                                                <small className="text-muted">SST Nodes</small>
                                                <br />
                                                <Link to="/app/monitor/node">
                                                    <strong className="h4">{this.props.nodes.length}</strong>
                                                </Link>
                                            </div>
                                        </Col>
                                        <Col sm={4}>
                                            <div className="callout callout-info">
                                                <small className="text-muted">SST Blocks</small>
                                                <br />
                                                <Link to="/app/monitor/blockchain">
                                                    <strong className="h4">{this.props.blocks.length}</strong>
                                                </Link>
                                            </div>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </main>
        );
    }
}


const mapStateToProps = state => ({
    names: R.values(state.app.name),
    nodes: R.values(state.entity.node),
    blocks: R.sort(
        (a, b) => b.height - a.height,
        R.values(state.entity.block),
    ),
    values: R.values(state.entity.value),
    ids: R.values(state.entity.id),
});

MonitorDashboard.propTypes = {
    send: PropTypes.func.isRequired,
    nodes: PropTypes.arrayOf(
        PropTypes.shape(constants.NODE_SHAPE),
    ).isRequired,
    blocks: PropTypes.arrayOf(
        PropTypes.shape(
            constants.BLOCK_SHAPE,
        ).isRequired,
    ),
    names: PropTypes.array.isRequired,
    values: PropTypes.array.isRequired,
    ids: PropTypes.array.isRequired,
};

export default connect(mapStateToProps, {send})(MonitorDashboard);
