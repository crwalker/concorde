import React from 'react';
import logo from '@/assets/img/logo.png';

export default props => (
    <div
        className="app flex-row align-items-center"
    >
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card-group mb-0">
                        <div className="card p-2 card-inverse card-primary">
                            {props.children}
                        </div>
                        <div
                            className="card py-3"
                            style={{
                                backgroundImage: `url(${logo})`,
                                backgroundRepeat: 'no-repeat',
                                backgroundSize: 'cover',
                                backgroundPosition: 'center',
                                width: '44%',
                            }}
                        >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
