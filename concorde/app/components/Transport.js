import React from 'react';
import PropTypes from 'prop-types';
import {Switch, Route, Link} from 'react-router-dom';

import TransportDashboard from '@/components/TransportDashboard';
import Search from '@/components/Search';


class Transport extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/app/transport" component={TransportDashboard} />
                <Route exact path="/app/transport/search" component={Search} />
            </Switch>
        );
    }
}

export default Transport;
