import React from 'react';
import PropTypes from 'prop-types';
import {Popover, OverlayTrigger} from 'react-bootstrap';
import copy from 'copy-to-clipboard';
import R from 'ramda';
import {connect} from 'react-redux';

import {setClientKeys} from '@/actions';
import Intro from '@/components/Intro';
import utils from '@/utils';
import constants from '@/constants';
import {sodium} from '@/crypto';

class RequireSetup extends React.Component {
    constructor(props) {
        super(props);

        const publicKey = utils.base16ToArray(localStorage.getItem('concorde-public-key')) || null;
        const secretKey = utils.base16ToArray(localStorage.getItem('concorde-secret-key')) || null;
        const clientKey = utils.base16ToArray(localStorage.getItem('concorde-client-key')) || null;
        const accepted = Boolean(localStorage.getItem('concorde-accept-tc'));

        this.state = {
            publicKey,
            secretKey,
            clientKey,
            accepted,
        };

        this.props.setClientKeys({
            clientKey,
            publicKey,
            secretKey,
        });
    }

    generate = () => {
        // identity of this node
        const clientKey = new Uint8Array(constants.KEY_BYTES);
        window.crypto.getRandomValues(clientKey);

        // public / private keypair for this node
        const {publicKey, privateKey} = R.compose(
            // R.map(utils.arrToBase16),
            R.pick(['publicKey', 'privateKey']),
        )(sodium.crypto_sign_keypair());

        localStorage.setItem('concorde-public-key', utils.arrToBase16(publicKey));
        localStorage.setItem('concorde-secret-key', utils.arrToBase16(privateKey));
        localStorage.setItem('concorde-client-key', utils.arrToBase16(clientKey));

        this.setState({
            clientKey,
            publicKey,
            secretKey: privateKey,
        });

        this.props.setClientKeys({
            clientKey,
            publicKey,
            secretKey: privateKey,
        });
    }

    accept = () => {
        localStorage.setItem('concorde-accept-tc', true);
        this.setState({accepted: true});
    }

    onClick = () => copy(this.state.secretKey);

    render() {
        const popover = (
            <Popover id="tooltip">Copied to clipboard</Popover>
        );

        if (!this.state.publicKey || !this.state.secretKey) {
            return (
                <Intro>
                    <div className="card-block">
                        <h1>Set Up Concorde</h1>
                        <p>Generate a secure key pair. The private key is saved to this browser and never sent to anyone else.</p>
                        <p><b>Make sure to save a backup copy of the secret key: it is the only way to access your data.</b></p>
                        <button
                            className="btn btn-danger btn-inverse"
                            onClick={this.generate}
                        >
                            Generate
                        </button>
                    </div>
                </Intro>
            );
        }

        if (!this.state.accepted) {
            return (
                <Intro>
                    <h2>Secret key</h2>
                    <form>
                        <div className="form-group">
                            <OverlayTrigger
                                placement="top"
                                overlay={popover}
                                trigger="click"
                                animation={false}
                                rootClose
                            >
                                <textarea
                                    readOnly
                                    rows={5}
                                    className="form-control"
                                    value={utils.arrToBase16(this.state.secretKey)}
                                    style={{wordWrap: 'break-word', backgroundColor: 'white'}}
                                    onClick={this.onClick}
                                />
                            </OverlayTrigger>
                        </div>
                    </form>
                    <p>Save this secret key before proceeding!</p>
                    <button
                        className="btn btn-danger btn-inverse"
                        onClick={this.accept}
                    >
                        Continue
                    </button>
                </Intro>
            );
        }

        return React.cloneElement(
            this.props.children,
            {
                clientKey: this.state.clientKey,
                secretKey: this.state.secretKey,
                publicKey: this.state.publicKey,
            },
        );
    }
}

RequireSetup.propTypes = {
    setClientKeys: PropTypes.func.isRequired,
};

export default connect(null, {setClientKeys})(RequireSetup);
