import React from 'react';
import PropTypes from 'prop-types';
import {Dropdown} from 'react-bootstrap';
import R from 'ramda';

class ItemMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    toggle = open => (open ? this.setState({open: true}) : null);

    render() {
        return (
            <Dropdown id="dropdown" {...R.omit('always', this.props)} onToggle={this.toggle}>
                <Dropdown.Toggle bsSize="xsmall" className="btn btn-outline btn-default">
                    <i className="icon-menu" />
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {this.state.open || this.props.always ?
                        this.props.children
                        :
                        null
                    }
                </Dropdown.Menu>
            </Dropdown>
        );
    }
}

ItemMenu.defaultProps = {
    always: false,
};

ItemMenu.propTypes = {
    always: PropTypes.bool,
};

export default ItemMenu;
