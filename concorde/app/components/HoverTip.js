import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import {Tooltip, OverlayTrigger, Popover} from 'react-bootstrap';


const OverWrapper = ({tipRef, ...props}) => (
    <Tooltip id="tooltip" {...props} ref={tipRef} />
);

OverWrapper.propTypes = {
    tipRef: PropTypes.func.isRequired,
};

const HoverTip = ({tip, tipRef, ...props}) => {
    const over = (
        <OverWrapper>
            {tip}
        </OverWrapper>
    );

    const overlay = <Popover id="tooltip">{tip}</Popover>;

    return (
        <OverlayTrigger
            placement="bottom"
            trigger="hover"
            animation={false}
            overlay={overlay}
            {...props}
        >
            <span>{props.children}</span>
        </OverlayTrigger>
    );
};

HoverTip.propTypes = {
    tip: PropTypes.node.isRequired,
    tipRef: PropTypes.func,
};

HoverTip.defaultProps = {
    tipRef: () => null,
};

export default HoverTip;
