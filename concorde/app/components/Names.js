import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';

import Breadcrumb from '@/components/Breadcrumb';
import PageHeader from '@/components/PageHeader';
import Explorer from '@/components/Explorer';
import {runningSearches} from '@/selectors';
import FindSidebar from '@/components/FindSidebar';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';


const Names = props => (
    <div className="app-body">
        <FindSidebar />
        <main className="main">
            <PageHeader>
                <h1 className="h2 page-title">Names</h1>
                <div className="text-muted page-desc">Personal labels for important things</div>
            </PageHeader>
            <Breadcrumb
                path={[
                    {name: 'Home', url: '/'},
                    {name: 'Find', url: '/app/find'},
                    {name: 'Names'},
                ]}
            />
            <div className="container-fluid">
                <Explorer keys={props.keys} />
            </div>
        </main>
        <Aside>
            <QueuedKeys />
            <Auth />
        </Aside>
    </div>
);

Names.propTypes = {
    keys: PropTypes.objectOf(PropTypes.string),
};

const mapStateToProps = state => ({
    keys: state.app.name,
    searches: runningSearches(state, R.keys(state.app.name)),
});

export default connect(mapStateToProps)(Names);
