import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import R from 'ramda';
import {Redirect} from 'react-router';
import QrReader from 'react-qr-reader';
import {Switch, Route, Link, withRouter} from 'react-router-dom';

import FindSidebar from '@/components/FindSidebar';
import FindDashboard from '@/components/FindDashboard';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Names from '@/components/Names';
import Identities from '@/components/Identities';
import Values from '@/components/Values';
import Explorer from '@/components/Explorer';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import {send} from '@/actions';
import utils from '@/utils';
import apps from '@/apps';
import Auth from '@/components/Auth';
import KeyInput from '@/components/KeyInput';
import Search from '@/components/Search';

const Find = () => (
    <Switch>
        <Route exact path={'/app/find/'} component={FindDashboard} />
        <Route path={'/app/find/key/:key?'} component={Search} />
        <Route path={'/app/find/name'} component={Names} />
        <Route path={'/app/find/id'} component={Identities} />
        <Route path={'/app/find/value'} component={Values} />
    </Switch>
);

export default Find;
