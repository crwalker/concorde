import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import R from 'ramda';
import {Switch, Route, Link} from 'react-router-dom';

import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Node from '@/components/Node';


const Key = () => (
    <main className="main">
        <PageHeader>
            <h1>Key</h1>
            <p>Find anything in Concorde</p>
        </PageHeader>
        <Breadcrumb path={[{name: 'key'}]} />
        <div className="container-fluid">
            Not sure what to do with you
        </div>
    </main>
);

export default connect(null)(Key);
