import React from 'react';
import R from 'ramda';
import PropTypes from 'prop-types';
import {Form, FormGroup, ControlLabel, FormControl, Grid, Row, Col, Button} from 'react-bootstrap';
import jsonp from 'jsonp-promise';

class MailingList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sent: false,
            success: false,
            errors: [],
            email: '',
        };
    }

    emailValid = email => email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);;

    onSubmit = (event) => {
        event.preventDefault();

        if (!this.emailValid(this.state.email)) {
            this.setState({errors: ['invalid email address']});
            return;
        }

        this.setState({sent: true});

        const url = `//ckwalker.us12.list-manage.com/subscribe/post-json?u=db0c78f1515fbd3c0ac85ea1d&id=891604016d&EMAIL=${this.state.email}`;

        const opts = {
            param: 'c',
        };

        jsonp(url, opts).promise.then(
            () => this.setState({sent: true, success: true, errors: []}),
        ).catch(
            res => this.setState({errors: R.append(res.text(), this.state.errors)}),
        );
    };

    render() {
        return (
            <Form inline>
                <FormGroup controlId="email">
                    <FormControl
                        placeholder="Your Email Address"
                        type="email"
                        value={this.state.email}
                        onChange={event => this.setState({email: event.target.value})}
                        style={{border: '1px solid white'}}
                    />
                </FormGroup>
                <Button
                    className="btn btn-primary"
                    type="submit"
                    onClick={this.onSubmit}
                    disabled={this.state.sent}
                    style={{borderColor: 'rgba(0, 0, 0, 0)'}}
                >
                    Subscribe
                </Button>
                <span style={{color: 'white'}}>
                    &nbsp;
                    {this.state.success && 'Almost finished - please click the link in the email we just sent you.'}
                    {!R.isEmpty(this.state.errors) && R.map(error => <span key={error}>{error}</span>, this.state.errors)}
                </span>
            </Form>
        );
    }
    //     return (
    //     <div>
    //         <div id="mc_embed_signup">
    //             <form
    //                 action="//ckwalker.us12.list-manage.com/subscribe/post?u=db0c78f1515fbd3c0ac85ea1d&amp;id=891604016d"
    //                 method="post"
    //                 id="mc-embedded-subscribe-form"
    //                 name="mc-embedded-subscribe-form"
    //                 className="validate"
    //                 target="_blank"
    //                 noValidate
    //             >
    //                 <div id="mc_embed_signup_scroll">
    //                     <h2>Subscribe</h2>
    //                     <div class="mc-field-group">
    //                         <label htmlFor="mce-EMAIL">Email Address</label>
    //                         <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" />
    //                     </div>
    //                     <div id="mce-responses" class="clear">
    //                         <div class="response" id="mce-error-response" style="display:none" />
    //                         <div class="response" id="mce-success-response" style="display:none" />
    //                     </div>
    //                     <div class="clear">
    //                         <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button" />
    //                     </div>
    //                 </div>
    //             </form>
    //         </div>
    //         {/* <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js' />
    //         <script type='text/javascript'>
    //             (function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
    //         */}
    //     </div>
    // );
}

export default MailingList;
