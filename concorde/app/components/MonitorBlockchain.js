import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';

import {send} from '@/actions';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Block from '@/components/Block';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Paginator, {withPaginator} from '@/components/Paginator';
import Loading from '@/components/Loading';


const Blocks = props => (
    props.keys.length > 0 ?
        <div>
            <Paginator.Pager {...props} />
            {R.map(
                block => (
                    <Card key={block.key} >
                        <Card.Body>
                            <Block block={block} />
                        </Card.Body>
                    </Card>
                ),
                props.blocks,
            )}
            <Paginator.Pager {...props} />
        </div>
        :
        <Loading />
);

const mapStateToProps2 = (state, ownProps) => ({
    blocks: R.map(
        key => state.entity.block[key],
        ownProps.keys,
    ),
});

Blocks.propTypes = {
    keys: PropTypes.arrayOf(PropTypes.string).isRequired,
    blocks: PropTypes.arrayOf(PropTypes.shape(constants.BLOCK_SHAPE)).isRequired,
};

const WrappedBlocks = withPaginator(connect(mapStateToProps2)(Blocks));


class MonitorBlockchain extends React.Component {
    render() {
        return (
            <main className="main">
                <PageHeader>
                    <h1 className="h2 page-title">Blockchain</h1>
                    <div className="text-muted page-desc">View all blocks in the blockchain</div>
                </PageHeader>
                <Breadcrumb
                    path={[
                        {name: 'Home', url: '/'},
                        {name: 'Monitor SST', url: '/app/monitor'},
                        {name: 'Blockchain'},
                    ]}
                />
                <div className="container-fluid">
                    <WrappedBlocks keys={this.props.blockKeys} blocks={this.props.blocks} />
                </div>
            </main>
        );
    }
}


const mapStateToProps = (state) => {
    const blockKeys = R.compose(
        R.map(block => block.key),
        R.sort((a, b) => b.height - a.height),
        R.values,
    )(state.entity.block);

    return ({
        blockKeys,
    });
};

MonitorBlockchain.propTypes = {
    send: PropTypes.func.isRequired,
    blockKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default connect(mapStateToProps, {send})(MonitorBlockchain);
