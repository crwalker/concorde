import React from 'react';

const Card = ({children}) => (
    <div className="card">
        {children}
    </div>
);


Card.Header = ({children}) => (
    <div className="card-header">
        {children}
    </div>
);

Card.Body = ({children}) => (
    <div className="card-block">
        {children}
    </div>
);

export default Card;
