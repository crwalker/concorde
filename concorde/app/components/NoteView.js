import React from 'react';
import ReactMarkdown from 'react-markdown';
import PropTypes from 'prop-types';
import {Row, Col, Button} from 'react-bootstrap';

import Card from '@/components/Card';
import EntryButton from '@/components/EntryButton';
import constants from '@/constants';
import {Link} from 'react-router-dom';

const NoteView = ({note}) => (
    <Card>
        <Card.Body>
            {note.key ?
                <div className="float-right">
                    <EntryButton k={note.key} />
                </div>
                :
                null

            }
            <ReactMarkdown source={note.data} escapeHtml />
            <Link to={`/app/note/edit/${note.key}`}>
                <Button
                    className="btn btn-outline-primary btn-default"
                >
                    New Version
                </Button>
            </Link>
        </Card.Body>
    </Card>
);

NoteView.propTypes = {
    note: PropTypes.shape(constants.VALUE_SHAPE),
};

export default NoteView;
