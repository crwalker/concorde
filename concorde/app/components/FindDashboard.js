import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';
import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {send} from '@/actions';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import FindSidebar from '@/components/FindSidebar';

class FindDashboard extends React.Component {
    render() {
        return (
            <div className="app-body">
            <FindSidebar />
                <main className="main">
                    <PageHeader>
                        <h1 className="h2 page-title">Find</h1>
                        <div className="text-muted page-desc">Locate your data</div>
                    </PageHeader>
                    <Breadcrumb
                        path={[
                            {name: 'Home', url: '/'},
                            {name: 'Find'},
                        ]}
                    />
                    <div className="container-fluid">
                        <Row>
                            <Col sm={6}>
                                <Card>
                                    <Card.Header>
                                        My Data
                                    </Card.Header>
                                    <Card.Body>
                                        <Row>
                                            <Col sm={4}>
                                                <div className="callout callout-info">
                                                    <small className="text-muted">Names</small>
                                                    <br />
                                                    <Link to="/app/find/name">
                                                        <strong className="h4">{this.props.names.length}</strong>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col sm={4}>
                                                <div className="callout callout-info">
                                                    <small className="text-muted">Identities</small>
                                                    <br />
                                                    <Link to="/app/find/id">
                                                        <strong className="h4">{this.props.ids.length}</strong>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col sm={4}>
                                                <div className="callout callout-info">
                                                    <small className="text-muted">Values</small>
                                                    <br />
                                                    <Link to="/app/find/value">
                                                        <strong className="h4">{this.props.values.length}</strong>
                                                    </Link>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </main>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    names: R.values(state.app.name),
    values: R.values(state.entity.value),
    ids: R.values(state.entity.id),
});

FindDashboard.propTypes = {
    send: PropTypes.func.isRequired,
    names: PropTypes.arrayOf(PropTypes.shape(constants.NAME_SHAPE)).isRequired,
    values: PropTypes.arrayOf(PropTypes.shape(constants.VALUE_SHAPE)).isRequired,
    ids: PropTypes.arrayOf(PropTypes.shape(constants.ID_SHAPE)).isRequired,
};

export default connect(mapStateToProps, {send})(FindDashboard);
