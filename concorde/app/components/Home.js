import React from 'react';
import PropTypes from 'prop-types';
import constants from '@/constants';
import {Row, Col, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import R from 'ramda';

import Breadcrumb from '@/components/Breadcrumb';
import HomeSidebar from '@/components/HomeSidebar';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Aside from '@/components/Aside';
import Auth from '@/components/Auth';
import QueuedKeys from '@/components/QueuedKeys';
import sstLogo from '@/assets/img/sst.png';
import concordeLogo from '@/assets/img/logo.png';
import apps from '@/apps';

const Home = () => (
    <div className="app-body">
        <HomeSidebar />
        <main className="main">
            <div className="container-fluid" style={{marginTop: 24}}>
                <Row>
                    <Col md={6}>
                        <div className="card card-accent-danger">
                            <div className="card-block">
                                <Row>
                                    <Col sm={10}>
                                        <h2 style={{margin: 0}}>SST</h2>
                                        <div className="text-muted page-desc">Universal data store enabling a single source of truth</div>
                                    </Col>
                                    <Col sm={2}>
                                        <div className="float-right">
                                            <img alt="sst logo" src={sstLogo} className="img-fluid" style={{height:56, width: 56}} />
                                        </div>
                                    </Col>
                                </Row>
                                <hr />
                                <p>
                                    SST enables participating nodes to create a universal key-value store
                                    which reaches consensus by blockchain, verifies storage transactions
                                    using commutative hashing, and secures values by public-key cryptography,
                                    forming a decentralized storage network with other SST nodes.
                                </p>
                                <p>
                                    SST aims to create a simple, low-cost way to store and share data that always
                                    works. Ethernet always works for transmitting data, but we want a similarly
                                    ubiquitous and trustworthy system for storing data.
                                </p>
                                <p>
                                    SST enables software to do things that sound a bit like magic today:
                                </p>
                                <ul>
                                    <li>
                                        <strong>Traceability</strong>: want to track the complete history of every document? Done.
                                    </li>
                                    <li>
                                        <strong>Automatic updates</strong>: there's no need to attach the latest version of that presentation to the email chain. The intended recipients already have it -- in fact, you don't need the email either.
                                    </li>
                                    <li>
                                        <strong>Pre-integration</strong>: want your latest sales to be reflected on the company's website? The website and the CRM already talk the same language -- it's probably a one-line code change.
                                    </li>
                                    <li>
                                        <strong>Cross-company collaboration</strong>: need to share select data with business partners? SST keeps everyone on the same page without vendor lock-in or giving one company undue leverage over another.
                                    </li>
                                    <li>
                                        <strong>Simplicity</strong>: can you deploy software the same way you cache content, the same way you sync databases, the same way you back up critical assets, the same way you send emails? We think you can.
                                    </li>
                                </ul>
                                <p>
                                    This is a different way of thinking about software, so we're also building a proof-of-concept SST client called Concorde to demonstrate these ideas in action.
                                </p>
                                <p>
                                    To find out more about SST, please email chris@ckwalker.com.
                                    We already have a community of folks interested in tackling this problem,
                                    but we want your input on potential applications for this technology.
                                    Our goal is to build SST into a simple, general purpose, open source protocol that
                                    solves data collaboration problems in every industry.
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col md={6}>
                        <div className="card card-accent-primary">
                            <Card.Body>
                                <Row>
                                    <Col sm={10}>
                                        <h2 style={{margin: 0}}>Concorde</h2>
                                        <div className="text-muted page-desc">Simple reference web client for SST</div>
                                    </Col>
                                    <Col sm={2}>
                                        <div className="float-right">
                                            <img alt="concorde logo" src={concordeLogo} className="img-fluid" style={{height:56}} />
                                        </div>
                                    </Col>
                                </Row>
                                <hr />
                                <p>Concorde demonstrates some of the basic features of SST:</p>
                                <ul>
                                    <li>
                                        <strong>Immutable values</strong>: save a particular version of a document
                                    </li>
                                    <li>
                                        <strong>Mutable identities</strong>: track the complete history of a document
                                    </li>
                                    <li>
                                        <strong>Access</strong>: send a link to a document to a friend
                                    </li>
                                    <li>
                                        <strong>Collaboration</strong>: automatically keep all parties on the same page as the document changes
                                    </li>
                                </ul>
                                <p>The technology behind SST is universally applicable: SST has enough flexibility to replace email, file systems, cloud storage, databases, content distribution networks, etc.</p>
                                <p>Most of the value provided by software applications, however, is domain expertise in presentation
                                 and manipulation of data so that users can accomplish some specific task effectively.
                                 For instance, ordering a part on Amazon is a very different experience than selling a part on Amazon, even if both sides of the transaction rely on the same data model.
                                 The different parties need different user experiences to accomplish their goals.</p>
                                <p>Concorde includes the concept of apps, or modules designed around one specific domain. The demo apps are very simple (and most are not yet complete):</p>
                                <ul className="icons-list">
                                    {R.map(
                                        app => (
                                            <li key={app.key} >
                                                {app.icon}
                                                <div className="desc">
                                                    <div className="title">
                                                        <Link to={`/${app.url}`}>{app.title}</Link>
                                                    </div>
                                                    <small>{app.description}</small>
                                                </div>
                                            </li>
                                        ),
                                        R.values(apps),
                                    )}
                                </ul>
                                <br />
                                <p>We want to augment these apps with domain specific tools, e.g. for managing shipments or small business operations. The hope for Concorde is that descendants of these initial apps will provide enough value in their own right that they are adopted widely.</p>
                            </Card.Body>
                        </div>
                    </Col>
                </Row>
            </div>
        </main>
        <Aside>
            <QueuedKeys />
            <Auth />
        </Aside>
    </div>
);

export default Home;
