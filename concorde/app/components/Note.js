import React from 'react';
import PropTypes from 'prop-types';
import {Switch, Route, Link} from 'react-router-dom';

import NoteDashboard from '@/components/NoteDashboard';
import NoteCompose from '@/components/NoteCompose';


class Note extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/app/note" component={NoteDashboard} />
                <Route exact path="/app/note/compose/" component={NoteCompose} />
                <Route path="/app/note/edit/:k?" component={NoteCompose} />
            </Switch>
        );
    }
}

export default Note;
