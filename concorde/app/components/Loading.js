import React from 'react';
import Spinner from 'react-spinkit';

export default () => <Spinner name="double-bounce" color="#F86C6B" fadeIn="quarter" />;
