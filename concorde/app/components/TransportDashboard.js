import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {send} from '@/actions';
import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';
import TransportSidebar from '@/components/TransportSidebar';
import NoteView from '@/components/NoteView';
import Paginator, {withPaginator} from '@/components/Paginator';


class TransportDashboard extends React.Component {
    componentWillMount() {
        // need to fetch notes somehow
    }

    render() {
        return (
            <div className="app-body">
                <TransportSidebar />
                <main className="main">
                    <PageHeader>
                        <h1 className="h2 page-title">Transport</h1>
                        <div className="text-muted page-desc">Securely track goods</div>
                    </PageHeader>
                    <Breadcrumb
                        path={[
                            {name: 'Home', url: '/app'},
                            {name: 'Transport'},
                        ]}
                    />
                    <div className="container-fluid">
                        <Row>
                            <Col sm={6}>
                                <Card>
                                    <Card.Body>
                                        <h3>Welcome to Transport</h3>
                                        <p>
                                            Transport is a simple tool for managing your shipments. SST secures data enabling you and your business partners to work together with increased trust.
                                        </p>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </main>
                <Aside>
                    <QueuedKeys />
                    <Auth />
                </Aside>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

TransportDashboard.propTypes = {
    send: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {send})(TransportDashboard);
