import React from 'react';
import {NavLink} from 'react-router-dom';
import R from 'ramda';

import HoverTip from '@/components/HoverTip';
import apps, {activeApp} from '@/apps';

const HomeSidebar = () => (
    <div className="sidebar">
        <nav className="sidebar-nav">
            <ul className="nav">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app">
                        <i className="icon-home" />
                        Home
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">Apps</li>
                {R.map(
                    app => (
                        <li className="nav-item" key={app.key}>
                            <NavLink className="nav-link" to={`/${app.url}`}>{app.icon}{app.title}</NavLink>
                        </li>
                    ),
                    R.values(apps),
                )}
            </ul>
        </nav>
    </div>
);

export default HomeSidebar;
