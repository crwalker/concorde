import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-bootstrap';

import Card from '@/components/Card';
import EntryButton from '@/components/EntryButton';
import utils from '@/utils';
import constants from '@/constants';


const Node = ({node}) => (
    <Card>
        <Card.Body>
            <Row>
                <Col sm={6}>
                    <h3 className="card-title">Node</h3>
                </Col>
                <Col sm={6}>
                    <div className="float-right">
                        <EntryButton k={node.key} />
                    </div>
                </Col>
            </Row>
            <hr className="m-0" />
            <Row>
                <Col sm={12}>
                    <ul className="icons-list">
                        <li>
                            <i className="icon-energy bg-primary"></i>
                            <div className="desc">
                                <div className="title">{constants.NODE_TYPES[node.type].display}</div>
                                <small>Only SST nodes participate in the SST blockchain</small>
                            </div>
                            <div className="value">
                            </div>
                            <div className="actions">
                            </div>
                        </li>
                        <li>
                            <i className="icon-share bg-info"></i>
                            <div className="desc">
                                <div className="title">TCP Address</div>
                                <small>large, guaranteed messages</small>
                            </div>
                            <div className="value">
                                <div className="small text-muted">&nbsp;</div>
                                <strong>{node.tcpHost}:{node.tcpPort}</strong>
                            </div>
                            <div className="actions">
                            </div>
                        </li>
                        <li>
                            <i className="icon-share bg-info"></i>
                            <div className="desc">
                                <div className="title">UDP Address</div>
                                <small>quick messages</small>
                            </div>
                            <div className="value">
                                <div className="small text-muted">&nbsp;</div>
                                <strong>{node.udpHost}:{node.udpPort}</strong>
                            </div>
                            <div className="actions">
                            </div>
                        </li>
                        <li>
                            <i className="icon-clock bg-warning"></i>
                            <div className="desc">
                                <div className="title">Last message</div>
                                <small>Time of last contact attempt</small>
                            </div>
                            <div className="value">
                                <div className="small text-muted">{node.unansweredMessages > 0 ? `${node.unansweredMessages} unanswered messages` : <span>&nbsp;</span>}</div>
                                <strong>{utils.time2str(node.lastMessage)}</strong>
                            </div>
                            <div className="actions">
                            </div>
                        </li>
                        <li>
                            <i className="icon-clock bg-danger"></i>
                            <div className="desc">
                                <div className="title">Last reply</div>
                                <small>Time of last message response</small>
                            </div>
                            <div className="value">
                                <div className="small text-muted">{node.unanswered > 0 ? `${node.unansweredMessages} unanswered messages` : <span>&nbsp;</span>}</div>
                                <strong>{node.lastReply > 0 ? utils.time2str(node.lastReply) : 'never'}</strong>
                            </div>
                            <div className="actions">
                            </div>
                        </li>
                    </ul>
                </Col>
            </Row>
        </Card.Body>
    </Card>
);

Node.propTypes = {
    node: PropTypes.shape(constants.NODE_SHAPE).isRequired,
};

export default Node;
