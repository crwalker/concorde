import React from 'react';
import ReactDOM from 'react-dom';
import {Dropdown, MenuItem} from 'react-bootstrap';

class CustomToggle extends React.Component {
    handleClick = (e) => {
        e.preventDefault();
        this.props.onClick(e);
    }

    render() {
        return (
            <a className="nav-link dropdown-toggle" onClick={this.handleClick}>
                {this.props.children}
            </a>
        );
    }
}

class CustomMenu extends React.Component {
    onChange = e => this.setState({value: e.target.value});

    focusNext() {
        const input = ReactDOM.findDOMNode(this.input);
        if (input) {
            input.focus();
        }
    }

    render() {
        return (
            <div className="dropdown-menu dropdown-menu-right" style={{padding: ''}}>
                <ul className="list-unstyled">
                    {this.props.children}
                </ul>
            </div>
        );
    }
}

export {CustomMenu, CustomToggle};
