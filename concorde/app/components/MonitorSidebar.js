import React from 'react';
import {NavLink} from 'react-router-dom';

const MonitorSidebar = () => (
    <div className="sidebar">
        <nav className="sidebar-nav">
            <ul className="nav">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app">
                        <i className="icon-home" />
                        Home
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">Monitor</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/monitor">
                        <i className="icon-speedometer" />
                        Dashboard
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">SST Network</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/monitor/node">Nodes</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/monitor/blockchain">Blockchain</NavLink>
                </li>
            </ul>
        </nav>
    </div>
);

export default MonitorSidebar;
