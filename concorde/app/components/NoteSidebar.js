import React from 'react';
import {NavLink} from 'react-router-dom';

const FindSidebar = () => (
    <div className="sidebar">
        <nav className="sidebar-nav">
            <ul className="nav">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app">
                        <i className="icon-home" />
                        Home
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">Note</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/note">
                        <i className="icon-notebook" />
                        Notes
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/note/compose">
                        <i className="icon-magnifier" />
                        Compose
                    </NavLink>
                </li>
            </ul>
        </nav>
    </div>
);

export default FindSidebar;
