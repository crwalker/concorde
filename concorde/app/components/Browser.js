import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Switch, Route, withRouter} from 'react-router-dom';
import R from 'ramda';
import cx from 'classnames';

import {apiStartWebsocket, send, loadKeys} from '@/actions';
import Header from '@/components/Header';
import Footer from '@/components/Footer';
import Home from '@/components/Home';
import NotFound from '@/components/NotFound';
import apps, {activeApp} from '@/apps';
import RequireSetup from '@/components/RequireSetup';


class Browser extends React.Component {
    componentWillMount() {
        // Start our initial API calls to get users and initial data
        this.props.apiStartWebsocket();
        this.props.send({
            type: 'blockchainRequest',
        });
        this.props.loadKeys();
    }

    render() {
        const active = activeApp(this.props.location);
        return (
            <div
                className={cx(
                    'app header-fixed sidebar-fixed aside-menu-fixed',
                    this.props.asideOpen ? '' : 'aside-menu-hidden',
                    this.props.sidebarOpen ? '' : 'sidebar-hidden',
                )}
            >
                <Header active={active} />
                <Switch>
                    <Route exact path="/app" component={Home} />
                    {R.map(
                        app => <Route path={`/${app.url}`} component={app.component} key={app.key} />,
                        R.values(apps),
                    )}
                    <Route component={NotFound} />
                </Switch>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    asideOpen: R.clone(state.app.asideOpen),
    sidebarOpen: R.clone(state.app.sidebarOpen),
});

Browser.propTypes = {
    apiStartWebsocket: PropTypes.func.isRequired,
    send: PropTypes.func.isRequired,
    asideOpen: PropTypes.bool.isRequired,
    sidebarOpen: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
    clientKey: PropTypes.object.isRequired,
    loadKeys: PropTypes.func.isRequired,
};

const withRequireSetup = Component => props => (
    <RequireSetup {...props}>
        <Component {...props} />
    </RequireSetup>
);


export default R.compose(
    withRouter,
    withRequireSetup,
    connect(mapStateToProps, {apiStartWebsocket, send, loadKeys}),
)(Browser);
