import React from 'react';
import PropTypes from 'prop-types';
import {Tooltip, OverlayTrigger, Popover, Form, Button, ButtonGroup, MenuItem} from 'react-bootstrap';
import R from 'ramda';
import copy from 'copy-to-clipboard';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';

import {version, addQueuedKey, removeQueuedKey} from '@/actions';
import utils from '@/utils';
import Tip from '@/components/Tip';
import ItemMenu from '@/components/ItemMenu';


const VersionItem = () => (
    <MenuItem onClick={() => version()}>
        <i className="icon-layers" /> Versions
    </MenuItem>
);

const EntryButton = (props) => {
    const popover = (
        <Popover id="tooltip">Copied to clipboard</Popover>
    );

    const onClick = () => {
        if (!props.queued) {
            props.addQueuedKey(props.k);
        }
        copy(utils.key2url(props.k));
    };

    return (
        <span>
            <ButtonGroup bsSize={props.bsSize}>
                <OverlayTrigger
                    placement="top"
                    overlay={popover}
                    trigger="click"
                    animation={false}
                    rootClose
                >
                    <Button
                        className="btn btn-outline-primary"
                        onClick={onClick}
                    >
                        <i className="icon-location-pin" />
                        &nbsp;
                        {<span style={{fontFamily: 'monospace'}}>{utils.key2display(props.k)}</span>}
                    </Button>
                </OverlayTrigger>
                <ItemMenu>
                    <MenuItem
                        onClick={() => props.history.push(utils.key2path(props.k))}
                    >
                        <i className="icon-magnifier" /> Find
                    </MenuItem>
                    {props.VersionItem()}
                    {props.queued ?
                        <MenuItem
                            onClick={() => props.removeQueuedKey(props.k)}
                        >
                            <i className="icon-close" /> Close
                        </MenuItem>
                        :
                        null
                    }
                </ItemMenu>
            </ButtonGroup>
        </span>
    );
};

EntryButton.defaultProps = {
    bsSize: 'sm',
    queued: false,
    VersionItem,
};

EntryButton.propTypes = {
    k: PropTypes.string.isRequired,
    addQueuedKey: PropTypes.func.isRequired,
    removeQueuedKey: PropTypes.func.isRequired,
    bsSize: PropTypes.string.isRequired,
    queued: PropTypes.bool,
    VersionItem: PropTypes.func,
};

export default withRouter(connect(R.identity, {addQueuedKey, removeQueuedKey})(EntryButton));
