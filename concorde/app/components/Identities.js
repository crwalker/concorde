import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import R from 'ramda';
import {Row, Col, Form, ControlLabel, FormGroup, InputGroup, FormControl, HelpBlock, Button} from 'react-bootstrap';

import constants from '@/constants';
import Breadcrumb from '@/components/Breadcrumb';
import Block from '@/components/Block';
import Card from '@/components/Card';
import PageHeader from '@/components/PageHeader';
import Explorer from '@/components/Explorer';
import {selectEntities, runningSearches} from '@/selectors';
import FindSidebar from '@/components/FindSidebar';
import Aside from '@/components/Aside';
import QueuedKeys from '@/components/QueuedKeys';
import Auth from '@/components/Auth';


const Identities = props => (
    <div className="app-body">
        <FindSidebar />
        <main className="main">
            <PageHeader>
                <h1 className="h2 page-title">Identities</h1>
                <div className="text-muted page-desc">Track anything that takes on different values over time</div>
            </PageHeader>
            <Breadcrumb
                path={[
                    {name: 'Home', url: '/'},
                    {name: 'Find', url: '/app/find'},
                    {name: 'Identities'},
                ]}
            />
            <div className="container-fluid">
                <Explorer keys={props.keys} />
            </div>
        </main>
        <Aside>
            <QueuedKeys />
            <Auth />
        </Aside>
    </div>
);

Identities.propTypes = {
    keys: PropTypes.arrayOf(PropTypes.string),
};

const mapStateToProps = state => ({
    keys: R.keys(state.identities),
    searches: runningSearches(state, R.keys(state.identities)),
});

export default connect(mapStateToProps)(Identities);
