import React from 'react';
import {NavLink} from 'react-router-dom';

const FindSidebar = () => (
    <div className="sidebar">
        <nav className="sidebar-nav">
            <ul className="nav">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app">
                        <i className="icon-home" />
                        Home
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">Find</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/find/">
                        <i className="icon-speedometer" />
                        Dashboard
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/find/key/">
                        <i className="icon-magnifier" />
                        Search
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="nav-title">Data Types</li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/find/name">Names</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/find/id">Identities</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/app/find/value">Values</NavLink>
                </li>
            </ul>
        </nav>
    </div>
);

export default FindSidebar;
