import React from 'react';
import PropTypes from 'prop-types';
import {Switch, Route, Link} from 'react-router-dom';

import constants from '@/constants';


class History extends React.Component {
    render() {
        return (
            <div>
                Some sweet history thing
            </div>
        );
    }
}

History.propTypes = {
    id: PropTypes.shape(constants.ID_SHAPE).isRequired,
};

export default History;
