import React from 'react';
import PropTypes from 'prop-types';
import {ControlLabel, FormGroup, InputGroup, FormControl, HelpBlock, Button} from 'react-bootstrap';
import R from 'ramda';

import constants from '@/constants';
import utils from '@/utils';
import Spinner from 'react-spinkit';

class KeyInput extends React.Component {

    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.props.keyString);
    }

    valid = () => utils.isKey(this.props.keyString);

    validationState = () => {
        if (this.props.keyString.length === 0) {
            return null;
        }

        return this.valid() ? 'success' : 'warning';
    };

    helpText = () => {
        if (this.props.keyString.length < 2 * types.KEY_BYTES) {
            return `address too short: must be ${2 * types.KEY_BYTES} characters`;
        } else if (this.props.keyString.length > 2 * types.KEY_BYTES) {
            return `address too long: must be ${2 * types.KEY_BYTES} characters`;
        } else if (!utils.isHexStr(this.props.keyString)) {
            return 'address may only contain the characters 0-9 and a-f';
        }
        return '';
    }

    render() {
        const searching = !R.isEmpty(R.filter(
            s => s.opts.targetKey === this.props.keyString && !s.ended,
            this.props.searches,
        ));

        return (
            <form onSubmit={this.onSubmit}>
                <FormGroup
                    controlId="formBasicText"
                    validationState={this.validationState()}
                >
                    <ControlLabel>Universal address search</ControlLabel>
                    <InputGroup>
                        <InputGroup.Addon>
                            <i className="icon-location-pin" />
                        </InputGroup.Addon>
                        <FormControl
                            type="text"
                            value={this.props.keyString}
                            onChange={(event) => this.props.onChange(event.target.value)}
                            maxLength={64}
                        />
                    </InputGroup>
                    <HelpBlock>{this.helpText()}</HelpBlock>
                </FormGroup>
                <Button
                    className="btn btn-primary btn-large centerButton"
                    type="submit"
                    disabled={searching || !this.valid()}
                >
                    {searching ? <Spinner name="double-bounce" color="#F86C6B" fadeIn="quarter" style={{margin: -5}} /> : 'Find'}
                </Button>
                &nbsp;
                <Button
                    className="btn btn-secondary btn-large centerButton"
                    onClick={() => this.props.onChange('')}
                    disabled={R.isEmpty(this.props.keyString)}
                >
                    Clear
                </Button>
            </form>
        );
    }
}

KeyInput.defaultProps = {
    searches: [],
};

KeyInput.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    keyString: PropTypes.string.isRequired,
    searches: PropTypes.arrayOf(PropTypes.shape(constants.MESSAGE_SHAPE)),
};


export default KeyInput;
