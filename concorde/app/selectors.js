import {createSelector} from 'reselect';
import R from 'ramda';

import utils from '@/utils';

// const getVisibilityFilter = state, props => state.visibilityFilter
// const getTodos = state => state.todos

// export const getVisibleTodos = createSelector(
//   [getVisibilityFilter, getTodos],
//   (visibilityFilter, todos) => {
//     switch (visibilityFilter) {
//       case 'SHOW_ALL':
//         return todos
//       case 'SHOW_COMPLETED':
//         return todos.filter(t => t.completed)
//       case 'SHOW_ACTIVE':
//         return todos.filter(t => !t.completed)
//     }
//   }
// )

export const getClientKey = state => state.app.clientKey;

export const selectEntities = (state, keys) => R.compose(
    R.filter(entity => Boolean(entity)),
    R.map(key => state.entity.node[key]
        || state.entity.id[key]
        || state.entity.value[key]
        || state.entity.block[key]
        || null,
    ),
    R.map(key => key),
)(keys);

// find any searches that match this key
export const runningSearches = (state, targetKeys) => R.values(R.filter(
    message => (
        message.opts.type === 'search'
        && !message.ended
        && R.contains(
            message.opts.targetkey,
            R.map(key => key, targetKeys),
        )
    ),
    state.app.message,
));
