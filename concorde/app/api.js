import R from 'ramda';
import msgpack from 'msgpack-lite';


import utils from '@/utils';
import {receiveEntities, addResponse, localSave} from '@/actions';
import constants from '@/constants';
import {valueHash, sodium, identitySignatureValid} from '@/crypto';

window.R = R;
window.msgpack = msgpack;

const operations = {};

const poolTimeout = 250;

const pool = (key, fn, objects) => {
    if (operations[key]) {
        operations[key].objects = R.concat(operations[key].objects, objects);
    } else {
        operations[key] = {
            objects,
            promise: utils.delayPromise(poolTimeout).then(
                () => {
                    const f = fn(operations[key].objects);
                    operations[key] = undefined;
                    return f;
                },
            ),
        };
    }
    return operations[key].promise;
};

const dataTypeHandlers = {
    markdown : {
        name: 'markdown',
        key: '6fa929f0f3819e181e70a03359fd827737bbae1e5d72a83c804be9f7980cb76f',
        encode: msgpack.encode,
        decode: msgpack.decode,
    },
    null: {
        name: null,
        key: '0000000000000000000000000000000000000000000000000000000000000000',
        encode: msgpack.encode,
        decode: msgpack.decode,
    },
};

const keysToString = obj => R.mapObjIndexed(
    // transform keys in incoming messages into encoded strings
    (value, key) => {
        if (R.is(Uint8Array, value) && (key.includes('key') || key.includes('Key') || key.includes('dataType'))) {
            return utils.arrToBase16(value);
        } else if (R.is(Array, value)) {
            return R.map(keysToString, value);
        } else if (R.is(Object, value) && !R.is(Array, value) && !R.is(Uint8Array, value)) {
            return keysToString(value);
        }
        return value;
    },
    obj,
);

const keysToBin = obj => R.mapObjIndexed(
    // transform key strings in outgoing messages into uint8arrays
    (value, key) => {
        if (R.is(String, value) && (key.includes('key') || key.includes('Key') || key.includes('dataType'))) {
            return utils.base16ToArray(value);
        } else if (R.is(Array, value)) {
            return R.map(keysToBin, value);
        } else if (R.is(Object, value) && !R.is(Array, value) && !R.is(Uint8Array, value)) {
            return keysToBin(value);
        }
        return value;
    },
    obj,
);

const messageTypeHandlers = {
    ping: {
        name: 'ping',
        prepare: R.identity,
        parse: () => {
            console.warn('got a ping');
        },
    },

    ack: {
        name: 'ack',
        parse: (ws, store, message) => {
            console.warn('got ack');

            if (!message.responseTo) {
                console.warn('Ack does not match any message we sent!');
                return;
            }

            store.dispatch(addResponse({
                message: message.responseTo,
                response: message,
            }));

            // TODO(cw): clean this up, verify that returned hash matches our calculated hash
            if (message.responseTo.opts.type === 'entryStore'
                && message.entryKey
                && utils.isKey(message.entryKey)
            ) {
                debugger;
                const newEntry = message.responseTo.opts.entries[0];

                if (newEntry.entryType === 'identity') {
                    store.dispatch(receiveEntities({
                        type: 'identity',
                        entity: {
                            key: message.responseTo.opts.targetKey,
                            value: newEntry.value,
                            previous: newEntry.previous,
                            publicKey: newEntry.publicKey,
                            signature: newEntry.signature,
                            live: newEntry.live,
                        },
                    }));
                } else if (newEntry.entryType === 'value') {
                    store.dispatch(receiveEntities([{
                        type: 'value',
                        entity: {
                            key: message.entryKey,
                            // TODO(cw): fix ack for multiple entry stores
                            data: newEntry.data,
                            dataType: newEntry.dataType,
                            live: true,
                        },
                    }]));
                }

                store.dispatch(localSave({
                    key: message.entryKey,
                    value: true,
                }));
            }
        },
    },

    search: {
        name: 'search',
        prepare: (message, opts) => {
            console.assert(opts.targetKey, 'search requires targetKey');
            message.targetKey = opts.targetKey;
            return message;
        },
    },

    nodeSearchResponse: {
        name: 'nodeSearchResponse',
        parse: (ws, store, message) => {
            console.warn('got node search response', message);
            console.assert(message.targetNode, 'message should have node');

            console.warn('pooling node search responses');
            pool(
                'incoming',
                x => store.dispatch(receiveEntities(x)),
                [{type: 'node', entity: message.targetNode}],
            );
        },
    },

    valueSearchResponse: {
        name: 'valueSearchResponse',
        parse: (ws, store, message) => {
            console.warn('got value search response');
            console.assert(message.entries, 'message must include entries');

            if (!message.responseTo) {
                console.warn('value search response does not match any message we sent!');
                return;
            }

            const entries = R.map(
                (e) => {
                    if (e.type === 'identity') {
                        return ({
                            type: 'identity',
                            entity: {
                                key: message.responseTo.opts.targetKey,
                                value: e.value,
                                previous: e.previous,
                                publicKey: utils.base16ToArray(e.publicKey),
                                signature: e.signature,
                                live: e.live,
                            },
                        });
                    } else if (e.type === 'value') {
                        const handler = R.compose(
                            R.head,
                            R.values,
                            R.filter(h => h.key === e.dataType),
                        )(dataTypeHandlers);

                        if (!handler) {
                            console.error(`unknown value entry datatype ${e.dataType}`);
                            debugger;
                        }

                        return ({
                            type: 'value',
                            entity: {
                                key: message.responseTo.opts.targetKey,
                                data: handler.decode(e.data),
                                dataType: handler.name,
                                live: true,
                            },
                        });
                    }
                    console.error('unknown entry type');
                    return undefined;
                },
                message.entries,
            );

            pool(
                'incoming',
                x => store.dispatch(receiveEntities(x)),
                entries,
            );
        },
    },

    entryStore: {
        name: 'entryStore',
        prepare: (message, opts) => {
            console.assert(opts.entries && R.is(Array, opts.entries), 'opts must include an array of entries');

            message.entries = R.map(
                (entry) => {
                    console.assert(constants.ENTRY_TYPES[entry.entryType], 'entry must include valid entry type');

                    const handler = dataTypeHandlers[entry.dataType];

                    if (entry.entryType === constants.ENTRY_TYPES.value.key) {
                        console.assert(
                            entry.dataType && handler && utils.isKey(handler.key),
                            'value entry must specify data and a known dataType',
                        );

                        const value = {
                            live: R.isNil(entry.live) ? true : !!entry.live,
                            type: entry.entryType,
                            dataType: handler.key,
                            data: handler.encode(entry.data),
                        };

                        value.key = valueHash(value);
                        return value;
                    } else if (entry.entryType === constants.ENTRY_TYPES.identity.key) {
                        const identity = R.pick(['key', 'value', 'previous', 'publicKey', 'signature'], entry);
                        identity.live = R.isNil(entry.live) ? true : !!entry.live;
                        identity.type = entry.entryType;

                        console.assert(identitySignatureValid(identity), 'identity signature is invalid');

                        return identity;
                    }

                    console.error('unknown entry type');
                    return undefined;
                },
                opts.entries,
            );

            return message;
        },
    },

    block: {
        name: 'block',
        parse: (ws, store, message) => {
            console.assert(message.block);

            const block = R.merge(
                message.block,
                {
                    key: utils.arrToBase16(message.block.hash),
                },
            );

            // console.log('got block', message.block);

            console.warn('pooling incoming blocks');
            pool(
                'incoming',
                x => store.dispatch(receiveEntities(x)),
                [{type: 'block', entity: block}],
            );
        },
    },

    blockchainRequest: {
        name: 'blockchainRequest',
        prepare: R.identity,
    },
};

const api = {
    send: (ws, store, opts) => (
        ws.ready.then(
            () => {
                let handler;
                try {
                    handler = messageTypeHandlers[opts.type];
                } catch (e) {
                    console.error('unknown message type', e);
                    return;
                }

                const message = handler.prepare(
                    {
                        sstVersion: 1,
                        type: opts.type,
                        key: opts.key || utils.randomKey(),
                        node: {
                            type: 'client',
                            key: utils.arrToBase16(store.getState().app.clientKey),
                        },
                    },
                    opts,
                );

                console.assert(constants.MESSAGE_TYPES[opts.type], 'opts type must be known message type');
                console.assert(message.key && utils.isKey(message.key), 'opts must include a valid message key');
                console.assert(message.node.key && utils.isKey(message.node.key), 'message must include a valid client key');

                console.warn('sending message: ', message);
                const transformed = keysToBin(message);
                // debugger;
                const encoded = msgpack.encode(transformed);
                console.warn('encoded as: ', R.map(a => a.toString(16), R.values(encoded)).join(' '));

                ws.socket.send(encoded);
            },
        ).catch(
            err => console.error('cannot send message: ', err),
        )
    ),

    receive: (ws, store, rawMessage) => {
        console.warn('received message');
        console.warn(
            R.map(
                a => a.toString(16),
                R.values(new Uint8Array(rawMessage.data)),
            ).join(' '),
        );

        let message = null;
        try {
            message = keysToString(msgpack.decode(new Uint8Array(rawMessage.data)));
            console.warn(message);
        } catch (e) {
            console.error('could not decode message', rawMessage.data);
            return;
        }

        if (message.sstVersion !== 1) {
            console.error('unknown sst version', message);
            return;
        } else if (!constants.MESSAGE_TYPES[message.type]) {
            console.error('unknown sst message type', message);
            return;
        } else if (!utils.isKey(message.key)) {
            console.error('invalid message key', message);
            return;
        } else if (
            !message.node
            || !constants.NODE_TYPES[message.node.type]
            || !utils.isKey(message.node.key)
        ) {
            console.error('from invalid sst node', message);
            return;
        }

        const handler = messageTypeHandlers[message.type];
        if (!handler) {
            console.error('missing handler for this message type', message);
            return;
        }

        message.responseTo = store.getState().app.message[message.key] || null;
        handler.parse(ws, store, message);
    },
};

window.send = msg => window.ws.socket.send(msgpack.encode(msg));

export {dataTypeHandlers, messageTypeHandlers};
export default api;

