import React from 'react';
import R from 'ramda';

import Transport from '@/components/Transport';
import Find from '@/components/Find';
import Monitor from '@/components/Monitor';
import Note from '@/components/Note';
import NotFound from '@/components/NotFound';

const apps = {
    monitor: {
        url: 'app/monitor',
        key: 'monitor',
        icon: <i className="icon-graph" />,
        title: <span>Monitor SST</span>,
        description: 'view the status of the SST network, for those who want to peek under the hood',
        enabled: true,
        component: Monitor,
    },
    find: {
        url: 'app/find',
        key: 'find',
        icon: <i className="icon-compass" />,
        title: <span>Find</span>,
        description: 'search the SST network for a particular key, display results, and make changes',
        enabled: true,
        component: Find,
    },
    note: {
        url: 'app/note',
        key: 'note',
        icon: <i className="icon-note" />,
        title: <span>Note</span>,
        description: 'write text, saving each version',
        enabled: false,
        component: Note,
    },
    transport: {
        url: 'app/transport',
        key: 'transport',
        icon: <i className="icon-globe-alt" />,
        title: <span>Transport</span>,
        description: 'Securely track goods',
        enabled: false,
        component: Transport,
    },
    message: {
        url: 'app/message',
        key: 'message',
        icon: <i className="icon-envelope" />,
        title: <span>Message</span>,
        description: 'send messages with values or identities to a friend',
        enabled: false,
        component: NotFound,
    },
    transact: {
        url: 'app/transact',
        key: 'transact',
        icon: <i className="icon-credit-card" />,
        title: <span>Transact</span>,
        description: 'buy, sell, or send value using the SST token',
        enabled: false,
        component: NotFound,
    },
    calculate: {
        url: 'app/calculate',
        key: 'calculate',
        icon: <i className="icon-calculator" />,
        title: <span>Calculate</span>,
        description: 'do math on tables of numbers as they change',
        enabled: false,
        component: NotFound,
    },

};

const activeApp = location => R.compose(
    R.head,
    R.keys,
    R.filter(app => location.pathname.startsWith(app.url, 1)),
)(apps) || null;

export {activeApp};

export default apps;
