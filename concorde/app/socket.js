import api from '@/api';
import {apiStartWebsocket, apiSendWebsocket} from '@/actions';

const ws = {};

window.ws = ws;

const createWebsocket = (callbacks) => {
    ws.ready = new Promise((resolve, reject) => {
        console.log('running promise');

        ws.socket = new WebSocket(`ws://${process.env.CONCORDE_HOST}:${process.env.CONNECTOR_WS_PORT}`);

        ws.socket.binaryType = 'arraybuffer';

        ws.socket.onmessage = (msg) => {
            if (callbacks.onmessage) {
                callbacks.onmessage(msg);
            }
        };

        ws.socket.onclose = () => {
            if (callbacks.onclose) {
                callbacks.onclose();
            }

            console.warn('websocket closed - will try to restart in 5 seconds');
            setTimeout(
                () => createWebsocket(callbacks).catch(err => console.warn('start websocket failed again: ', err)),
                5000,
            );
        };

        ws.socket.onopen = () => {
            console.warn('websocket opened!');
            resolve('websocket ready');
            if (callbacks.onopen) {
                callbacks.onopen();
            }
            resolve(ws.socket);
        };

        ws.socket.onerror = (err) => {
            console.warn('websocket error!');
            if (callbacks.onerror) {
                callbacks.onerror();
            }
            reject(err);
        };
    });
    return ws.ready;
};

const socketMiddleware = store => next => (action) => {
    switch (action.type) {
        case apiStartWebsocket().type:
            // connect to a server via websocket and start listening
            return createWebsocket({
                onmessage: (_store => msg => api.receive(ws, _store, msg))(store),
                onclose: null,
                onopen: null,
                onerror: null,
            }).catch(
                err => console.warn('could not start websocket: ', err),
            );

        case apiSendWebsocket().type:
            return api.send(ws, store, action.payload);

        default:
            // Pass any other actions on to the next middleware
            return next(action);
    }
};

export {socketMiddleware};
export default ws;
