/* eslint import/no-mutable-exports: "off", camelcase: "off" */
import _sodium from 'libsodium-wrappers';

import utils from '@/utils';
import constants from '@/constants';

let sodium;

(async () => {
    // ALWAYS wait for sodium to be ready before using it
    await _sodium.ready;
    sodium = _sodium;

    // let key = sodium.crypto_secretstream_xchacha20poly1305_keygen();

    // let res = sodium.crypto_secretstream_xchacha20poly1305_init_push(key);
    // let [state_out, header] = [res.state, res.header];
    // let c1 = sodium.crypto_secretstream_xchacha20poly1305_push(state_out,
    //     sodium.from_string('sodium test message 1'), null,
    //     sodium.crypto_secretstream_xchacha20poly1305_TAG_MESSAGE);
    // let c2 = sodium.crypto_secretstream_xchacha20poly1305_push(state_out,
    //     sodium.from_string('sodium test message 2'), null,
    //     sodium.crypto_secretstream_xchacha20poly1305_TAG_FINAL);

    // let state_in = sodium.crypto_secretstream_xchacha20poly1305_init_pull(header, key);
    // let r1 = sodium.crypto_secretstream_xchacha20poly1305_pull(state_in, c1);
    // let [m1, tag1] = [sodium.to_string(r1.message), r1.tag];
    // let r2 = sodium.crypto_secretstream_xchacha20poly1305_pull(state_in, c2);
    // let [m2, tag2] = [sodium.to_string(r2.message), r2.tag];

    // console.log(m1);
    // console.log(m2);
})();


// const newKeyPair = () => {
//     let publicKey, privateKey = sodium.crypto_sign_keypair();
// };

const serializeIdentity = (identity) => {
    const message = new Uint8Array(constants.KEY_BYTES + constants.HASH_BYTES + constants.PUBLIC_KEY_BYTES);
    message.set(identity.value, 0);
    message.set(identity.previous || new Uint8Array(constants.HASH_BYTES), constants.KEY_BYTES);
    message.set(identity.publicKey, constants.KEY_BYTES + constants.HASH_BYTES);

    console.log(message);

    return message;
};

const identitySignature = (identity, secretKey) => {
    const message = serializeIdentity(identity);

    const signature = sodium.crypto_sign_detached(message, secretKey);
    console.log(signature);
    return signature;
};

function identitySignatureValid(identity) {
    const message = serializeIdentity(identity);

    return sodium.crypto_sign_verify_detached(
        identity.signature,
        message,
        identity.publicKey);
}

const identityKey = (identity) => {
    const state = sodium.crypto_generichash_init(null, constants.HASH_BYTES);
    sodium.crypto_generichash_update(state, identity.value);
    sodium.crypto_generichash_update(state, identity.publicKey);
    sodium.crypto_generichash_update(state, identity.signature);

    const key = utils.arrToBase16(sodium.crypto_generichash_final(state, constants.HASH_BYTES));

    console.log('key: ', key);
    return key;
};

const identityHash = (identity) => {
    const state = sodium.crypto_generichash_init(null, constants.HASH_BYTES);

    sodium.crypto_generichash_update(state, identity.value);
    if (identity.previous) {
        sodium.crypto_generichash_update(state, identity.previous);
    } else {
        sodium.crypto_hash_update(state, new Uint8Array(constants.KEY_BYTES));
    }
    sodium.crypto_generichash_update(state, identity.publicKey);
    sodium.crypto_generichash_update(state, identity.signature);

    const hash = sodium.crypto_generichash_final(state, constants.HASH_BYTES);
    console.log('identity hash', utils.arrToBase16(hash));
    return hash;
};

const valueHash = (value) => {
    console.assert(value.data && value.dataType, 'value needs to have data and datatype');

    const state = sodium.crypto_generichash_init(null, constants.HASH_BYTES);

    sodium.crypto_generichash_update(state, utils.base16ToArray(value.dataType));
    sodium.crypto_generichash_update(state, value.data);

    const hash = sodium.crypto_generichash_final(state, constants.HASH_BYTES);
    console.log('value hash', utils.arrToBase16(hash));
    return hash;
};

const valueKey = value => utils.arrToBase16(valueHash(value));

const hash = arr => sodium.crypto_generichash(arr, null);

export {
    identityKey,
    identitySignature,
    identitySignatureValid,
    sodium,
    identityHash,
    valueHash,
    valueKey,
    hash
};

// export default sodium;
