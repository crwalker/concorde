import moment from 'moment';
import constants from '@/constants';
import R from 'ramda';

// function buf2str(buf) {
//     // convert a buffer containing utf8 to a javascript string
//     const encodedString = String.fromCharCode.apply(null, new Uint8Array(buf)),
//         decodedString = decodeURIComponent(escape(encodedString));
//     return decodedString;
// }

// function str2buf(str) {
//     // convert a native javascript string to arraybuffer containing utf8-encoded string
//     const utf8 = [];
//     for (let i = 0; i < str.length; i++) {
//         let charcode = str.charCodeAt(i);
//         if (charcode < 0x80) {
//             utf8.push(charcode);
//         } else if (charcode < 0x800) {
//             utf8.push(0xc0 | (charcode >> 6),
//                       0x80 | (charcode & 0x3f));
//         } else if (charcode < 0xd800 || charcode >= 0xe000) {
//             utf8.push(0xe0 | (charcode >> 12),
//                       0x80 | ((charcode >> 6) & 0x3f),
//                       0x80 | (charcode & 0x3f));
//         } else {
//             i++;
//             // surrogate pair
//             // UTF-16 encodes 0x10000-0x10FFFF by
//             // subtracting 0x10000 and splitting the
//             // 20 bits of 0x0-0xFFFFF into two halves
//             charcode = 0x10000 + (((charcode & 0x3ff) << 10)
//                       | (str.charCodeAt(i) & 0x3ff));
//             utf8.push(0xf0 | (charcode >> 18),
//                       0x80 | ((charcode >> 12) & 0x3f),
//                       0x80 | ((charcode >> 6) & 0x3f),
//                       0x80 | (charcode & 0x3f));
//         }
//     }

//     return (new Uint8Array(utf8)).buffer;
// }

const base64toArray = (string) => {
    const raw = atob(string);
    const arr = new Uint8Array(new ArrayBuffer(raw.length));
    for (let i = 0; i < raw.length; i++) {
        arr[i] = raw.charCodeAt(i);
    }
    return arr;
};

const arrToBase64 = arr => arr.toString('base64');

const base16ToArray = (string) => {
    // translate hex string into array buffer
    console.assert(string.length % 2 === 0, 'hex string must have even length');
    const nBytes = string.length / 2;
    const arr = new Uint8Array(nBytes);
    for (let i = 0; i < nBytes; i++) {
        arr[i] = parseInt(string.substr(2 * i, 2), 16);
    }
    return arr;
};

const arrToBase16 = arr => Array.prototype.map.call(
    arr,
    x => (`00${x.toString(16)}`).slice(-2),
).join('');

const time2str = (time) => {
    const date = new Date(1000 * time);
    return (date.getTime() > 0 ? moment(date).format() : ' - ');
};

const isHexStr = string => R.all(
    char => parseInt(char, 16).toString(16) === char.toLowerCase(),
    string,
);

// base 16 string
const isKey = str => /^[0-9a-fA-F]{64}$/.test(str);

// base64 string
// const isKey = str => /[^-A-Za-z0-9+/=]|=[^=]|={3,}$/.test(str);
// const isKey = str => /^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$/.test(str);

const randomKey = () => {
    const arr = new Uint8Array(constants.KEY_BYTES);
    window.crypto.getRandomValues(arr);
    return arrToBase16(arr);
};

const delayPromise = delay => new Promise(resolve => setTimeout(resolve, delay));

const key2display = key => key.slice(0, 6);

const arr2display = arr => arrToBase16(arr).slice(0, 6);

const key2path = key => `/app/find/key/${key}`;

const key2url = key => `http://${process.env.CONCORDE_HOST}/find/key/${key}`;

const path2url = path => `http://${process.env.CONCORDE_HOST}${path}`;

const data2display = (data) => {
    if (R.is(String, data)) {
        return data;
    } else if (R.is(Uint8Array, data)) {
        return `0x${arrToBase16(data)}`;
    } else if (R.is(Object, data)) {
        return R.toString(R.map(data2display, data));
    }
    return 'null';
};

const utils = {
    time2str,
    key2display,
    arr2display,
    isHexStr,
    key2path,
    key2url,
    isKey,
    randomKey,
    delayPromise,
    path2url,
    data2display,
    // string conversions
    arrToBase64,
    arrToBase16,
    base64toArray,
    base16ToArray,
};

export default utils;
