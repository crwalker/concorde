import R from 'ramda';
import {combineReducers} from 'redux';
import {handleActions} from 'redux-actions';

import utils from '@/utils';

import {
    startMessage,
    addResponse,
    endMessage,
    receiveEntities,
    setClientKeys,
    addQueuedKey,
    removeQueuedKey,
    toggleAside,
    toggleSidebar,
    addName,
} from '@/actions';

const entityReducer = handleActions({
    [receiveEntities]: (state, action) => R.mapObjIndexed(
        (entities, type) => Object.assign(
            entities,
            ...R.compose(
                R.map(object => ({[object.entity.key]: R.merge(
                    object.entity,
                    {_type: type},
                )})),
                R.filter(object => object.type === type),
            )(action.payload),
        ),
        state,
    ),
}, {
    node: {},
    identity: {},
    value: {},
    block: {},
});

const appReducer = handleActions({
    [addQueuedKey]: (state, action) => R.merge(state, {keyQueue: R.prepend(action.payload, state.keyQueue)}),
    [removeQueuedKey]: (state, action) => R.merge(state, {keyQueue: R.remove(action.payload, 1, state.keyQueue)}),
    [toggleAside]: state => R.merge(state, {asideOpen: !state.asideOpen}),
    [toggleSidebar]: state => R.merge(state, {sidebarOpen: !state.sidebarOpen}),
    [addName]: (state, action) => R.merge(state, {name: R.merge(state.name, action.payload)}),
    [startMessage]: (state, action) => R.merge(
        state,
        {message: R.merge(
            state.message,
            {[action.payload.opts.key]: action.payload},
        )},
    ),
    [addResponse]: (state, action) => {
        const s = R.clone(state);
        s.message[action.payload.message.opts.key].responses.push(action.payload.response);
        return s;
    },
    [endMessage]: (state, action) => {
        const s = R.clone(state);
        s.message[action.payload.opts.key].ended = true;
        return s;
    },
    [setClientKeys]: (state, action) => R.merge(state, action.payload),
}, {
    asideOpen: false,
    sidebarOpen: true,
    message: {},
    keyQueue: [],
    name: {},
    clientKey: null,
});

export default combineReducers({
    entity: entityReducer,
    app: appReducer,
});
