import PropTypes from 'prop-types';

const BOOL_BYTES = 1;
const ENUM_BYTES = 4;
const SIZE_BYTES = 8;
const TIME_BYTES = 8;
const KEY_BYTES = 32;
const HASH_BYTES = 32;
const STORAGE_PROOF_HASH_BYTES = 32;
const SIGNATURE_BYTES = 64;
const SECRET_KEY_BYTES = 64;
const PUBLIC_KEY_BYTES = 32;

const SEARCH_TIMEOUT = 5000;  // milliseconds

const KEY_PLACEHOLDER = `enter a ${2 * KEY_BYTES} character hex string`;

// Match SST enums
const NODE_TYPES = {
    null: {key: 'null', display: 'Unknown'},
    sst: {key: 'sst', display: 'SST Node'},
    client: {key: 'sst', display: 'Client'},
};

const ENTRY_TYPES = {
    value: {key: 'value', display: 'Value'},
    identity: {key: 'identity', display: 'Identity'},
};

const MESSAGE_TYPES = {
    ping: {key: 'ping', display: 'Ping'},
    ack: {key: 'ack', display: 'Ack'},
    search: {key: 'search', display: 'Search'},
    nodeSearchResponse: {key: 'nodeSearchResponse', display: 'Node Search Response'},
    valueSearchResponse: {key: 'valueSearchResponse', display: 'Value Search Response'},
    entryStore: {key: 'entryStore', display: 'Value Store'},
    block: {key: 'block', display: 'Block'},
    blockchainRequest: {key: 'blockchainRequest', display: 'Blockchain Request'},
};

const REFERENCE_SHAPE = {
    key: PropTypes.object,
};

const ENTITY_SHAPE = {
    key: PropTypes.string,
    type: PropTypes.instanceOf(ArrayBuffer),
};

const VALUE_SHAPE = Object.freeze({
    key: PropTypes.string.isRequired,
    value: PropTypes.instanceOf(ArrayBuffer),
});

const VALUES_SHAPE = PropTypes.objectOf(VALUE_SHAPE);

const ID_SHAPE = Object.freeze({
    id: PropTypes.string.isRequired,
    publicKey: PropTypes.string.isRequired,
    values: PropTypes.arrayOf(VALUE_SHAPE),
});

const SCRIPT_SHAPE = Object.freeze({
    type: PropTypes.string.isRequired,
    data: PropTypes.object,
});

const INPUT_SHAPE = Object.freeze({
    amount: PropTypes.number.isRequired,
    script: PropTypes.shape(SCRIPT_SHAPE).isRequired,
});

const NODE_SHAPE = Object.freeze({
    type: PropTypes.number.isRequired,
    streamHost: PropTypes.string.isRequired,
    streamPort: PropTypes.string.isRequired,
    datagramHost: PropTypes.string.isRequired,
    datagramPort: PropTypes.string.isRequired,
    messageTime: PropTypes.instanceOf(Date),
    replyTime: PropTypes.instanceOf(Date),
    messages: PropTypes.number.isRequired,
});

const NAME_SHAPE = Object.freeze({
    name: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
});

let MESSAGE_SHAPE;

MESSAGE_SHAPE = Object.freeze({
    opts: PropTypes.shape({
        key: PropTypes.string.isRequired,
        clientKey: PropTypes.object.isRequired,
        targetKey: PropTypes.string,
    }),
    ended: PropTypes.bool.isRequired,
    responses: PropTypes.arrayOf(PropTypes.shape(MESSAGE_SHAPE)),
});

const OUTPUT_SHAPE = Object.freeze({
    amount: PropTypes.number.isRequired,
    script: PropTypes.shape(SCRIPT_SHAPE).isRequired,
});

const TX_SHAPE = Object.freeze({
    inputs: PropTypes.arrayOf(PropTypes.shape(INPUT_SHAPE)),
    outputs: PropTypes.arrayOf(PropTypes.shape(OUTPUT_SHAPE)),
});

const BLOCK_SHAPE = Object.freeze({
    key: PropTypes.string,
    hash: PropTypes.object,
    txs: PropTypes.arrayOf(PropTypes.shape(TX_SHAPE)),
});

const IDS_SHAPE = PropTypes.objectOf(ID_SHAPE);

export default {
    BOOL_BYTES,
    ENUM_BYTES,
    SIZE_BYTES,
    TIME_BYTES,
    KEY_BYTES,
    HASH_BYTES,
    PUBLIC_KEY_BYTES,
    SECRET_KEY_BYTES,
    SIGNATURE_BYTES,
    STORAGE_PROOF_HASH_BYTES,
    NODE_TYPES,
    MESSAGE_TYPES,
    ENTRY_TYPES,
    KEY_PLACEHOLDER,
    ID_SHAPE,
    IDS_SHAPE,
    NAME_SHAPE,
    NODE_SHAPE,
    VALUE_SHAPE,
    VALUES_SHAPE,
    BLOCK_SHAPE,
    ENTITY_SHAPE,
    MESSAGE_SHAPE,
    REFERENCE_SHAPE,
    SEARCH_TIMEOUT,
};
