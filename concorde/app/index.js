import '@/assets/css/font-awesome.min.css';
import '@/assets/css/glyphicons-filetypes.css';
import '@/assets/css/glyphicons-social.css';
import '@/assets/css/glyphicons.css';
import '@/assets/css/simple-line-icons.css';
import '@/assets/css/style.css';

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import {Provider} from 'react-redux';
import {AppContainer} from 'react-hot-loader';

import utils from '@/utils';
import store from '@/store';
import Landing from '@/components/Landing';
import Browser from '@/components/Browser';
import RequireSetup from '@/components/RequireSetup';
import NotFound from '@/components/NotFound';

import App from '@/components/App';

const rootElement = document.getElementById('app');

// the redbox in browser is useless for debugging
delete AppContainer.prototype.unstable_handleError;

ReactDOM.render((
    <AppContainer>
        <Provider store={store}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>
    </AppContainer>
    ), rootElement);

if (module.hot) {
    module.hot.accept('./components/App', () => {
        ReactDOM.render((
            <AppContainer>
                <Provider store={store}>
                    <BrowserRouter>
                        <App />
                    </BrowserRouter>
                </Provider>
            </AppContainer>
        ), rootElement);
    });
}
