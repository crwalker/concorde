<img src="concorde/app/assets/img/logo.png" height="300">
# Concorde: Simple web client for SST
* View SST network data: nodes, blockchain
* Create and view values, identities, names
* Create and view transactions

## Setup
Get source code. Concorde uses git submodules to pull in source code for SST and Connector.
* `$ git clone git@gitlab.com:crwalker/concorde.git`
* `$ git submodule init && git submodule update` (get sst and connector source code)

Ensure that git, docker, and docker-compose are installed.
* `$ git -v`
* `$ docker -v`
* `$ docker-compose -v` (minimum version 1.13)

Ensure that environmental variables in `.env` match settings appropriate for host computer (e.g. adding an entry in /etc/hosts to match CONCORDE_HOST)
* `cp example.env .env'
* 'nano .env'

## Development
* `$ docker-compose up --build`

## Operation
* View in browser at CONCORDE_HOST:NGINX_PORT
* Attach to containers using `$docker exec -it 123 sh` where `123` is a container ID prefix
